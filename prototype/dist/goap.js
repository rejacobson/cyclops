// if the module has no dependencies, the above pattern can be simplified to
(function (root, factory) {
    if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like enviroments that support module.exports,
        // like Node.
        module.exports = factory();
    } else if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else {
        // Browser globals (root is window)
        root.GOAP = factory();
  }
}(this, function () {

    //////////////////////////////////////////
    // GOAP
    //////////////////////////////////////////
    var GOAP = (function() {
      var Graphs    = {};
      var Actionset = {};
      
      function hashString(str) {
        var hash = 0;
        if (str.length == 0) return hash;
        for (i = 0; i < str.length; i++) {
          char = str.charCodeAt(i);
          hash = ((hash<<5)-hash)+char;
          hash = hash & hash; // Convert to 32bit integer
        }
        return hash;
      };
      
      return {
        initialize: function(global_actionset) {
          Actionset = global_actionset;
        },

        getActionset: function() {
          return Actionset;
        },
        
        getGraph: function(actions) {
          if (!actions || actions.length == 0) return;

          var hash = hashString(actions.sort().join(''));
          
          if (Graphs[hash] === undefined) {
            var actionset = {};
            for (var i in actions) {
              actionset[actions[i]] = Actionset[actions[i]];
            }
            Graphs[hash] = new Graph(actionset);
          }
          
          return Graphs[hash];
        }    
      };
    })();
    
    //////////////////////////////////////////
    // State
    //////////////////////////////////////////
    var State = GOAP.State = function(initial) {
      this.keys = initial;
    };
    
    State.prototype = {
      forEachKey: function(callback, context) {
        context = context || this;
        var result;
        
        for (var i in this.keys) {
          if (this.keys[i] !== undefined) {
            result = callback.call(context, this.keys[i], i, this.keys);
            if (result !== undefined) return result;
          }
        }
      },
      
      clone: function() {
        var keys = {};
        this.forEachKey(function(value, key, list) {
          keys[key] = value;
        });
        return new State(keys);
      },
      
      distanceTo: function(state) {
        return Object.keys(state.difference(this).keys).length;
      },
      
      /**
       * Returns all of the keys in `this` that are not present in the comparison.
       *
       * a == [a, b, c, d];
       * b == [d, e, f];
       * a.difference(b) == [a, b, c];
       * b.difference(a) == [e, f]
       */
      difference: function(state) {
        var keys = {};
        var state = (state instanceof State) ? state.keys : state;
        
        this.forEachKey(function(value, key, list) {
          if (state[key] !== value) keys[key] = value;
        });
        
        return new State(keys);
      },
      
      /**
       * Returns a new State with the keys of each state merged.
       *
       * a == [a, b, c, d];
       * b == [d, e, f];
       * a.union(b) == [a, b, c, d, e, f];
       * b.union(a) == [d, e, f, a, b, c]
       */
      union: function(state) {
        var clone = this.clone();
        var state = (state instanceof State) ? state.keys : state;
        
        for (var key in state) {
          if (clone.keys[key] !== state[key]) clone.keys[key] = state[key];
        };
        
        return clone;
      }
    };
    
    //////////////////////////////////////////
    // Node
    //////////////////////////////////////////
    var Node = GOAP.Node = function(entity, id, goal, current) {
      this.entity  = entity;
      this.id      = id;
      
      this.g_score = 0;
      this.f_score = 0;
      this.h_score = 0;
      
      this.goal;
      this.current;
      
      this.setState(goal, current);
    }
    
    Node.prototype = {
      atGoal: function() {
        return this.current.distanceTo(this.goal) == 0;
      },
      
      setState: function(goal, current) {
        this.goal    = (goal instanceof State)    ? goal.clone()    : new State(goal);
        this.current = (current instanceof State) ? current.clone() : new State(current);
      },
      
      syncWithEntity: function() {
        var diff  = Object.keys(this.goal.difference(this.current));
        var state = this.entity.getState(diff);
        this.current = this.current.union(state); 
      },
      
      addAction: function(action) {
        this.current = this.current.union(action.effects); 
        this.goal    = this.goal.union(action.conditions);
        this.syncWithEntity();
      },
      
      diff: function() {
        return this.goal.difference(this.current);
      },

      distanceTo: function(node) {
        return this.goal.distanceTo(node.goal);
      }
    }
    
    //////////////////////////////////////////
    // Set
    //////////////////////////////////////////
    var Set = GOAP.Set = function() {
      this.items = [];
    };
    
    Set.prototype = {
      get length() {
        return this.items.length;
      },
      
      getBest: function() {
        var best;
        
        this.forEach(function(item, index, items) {
          if (!best || item.g_score < best.g_score) best = item;
        });
        
        return best;
      },
      
      forEach: function(callback, context) {
        context = context || this;
        var result;
        
        for (var i=0, len=this.items.length; i<len; ++i) {
          result = callback.call(context, this.items[i], i, this.items);
          if (result !== undefined) return result;
        }
      },
      
      insert: function(item) {
        this.items.push(item);
      },
      
      remove: function(item) {
        this.forEach(function(value, index, items) {
          if (item == value) {
            collection.splice(index, 1);
            return false;
          }
        });
      },
      
      has: function(item) {
        return this.forEach(function(value, index, items) {
          if (item == value) return true;
        });
      }  
    };
    
    //////////////////////////////////////////
    // Planner
    //////////////////////////////////////////
    var Planner = GOAP.Planner = function(entity, actions, goal) {
      this.entity = entity;
      this.agraph = GOAP.getGraph(actions);
      this.ngraph = {};
    };
    
    Planner.prototype = {
      getNeighborNodes: function(state) {
        var neighbors = this.getNeighborActions(state);
        var nodes = [];
        var action, id;
        
        for (var i in neighbors) {
          id = neighbors[i];
          if (this.getNode(id) === undefined) {
            action = this.getAction(id);
            if (!action) continue;
            this.ngraph[id] = new Node(this.entity, id, action.effects, {});
          }
          nodes.push(this.ngraph[id]);
        }
        
        return nodes;
      },
      
      getNeighborActions: function(state) {
        var diff = state.goal.difference(state.current);
        return this.agraph.getNeighbors(diff.keys);
      },
      
      getNode: function(id) {
        return this.ngraph[id];
      },
      
      getAction: function(id) {
        return this.agraph.getAction(id);
      },
      
      buildPlan: function() {
        var planner   = this;
        var openset   = new Set();
        var closedset = new Set();  
        var goal_node = new Node(entity, 'start', this.goal, {});
        
        goal_node.syncWithEntity();
        
        openset.insert(goal_node);
        
        while (openset.length) {
          current = openset.getBest();
          
          if (current.atGoal())
            return current; //reconstruct_path(current);
          
          openset.remove(current);
          closedset.insert(current);
          
          var neighbors = planner.getNeighborNodes(current),
              neighbor;
          
          for (var i in neighbors) {
            neighbor = neighbors[i];
            g_score = current.g_score + current.distanceTo(neighbor);
            
            if (closedset.has(neighbor) && g_score >= neighbor.g_score)
              continue;
              
            if (!openset.has(neighbor) || g_score < neighbor.g_score) {
              neighbor.parent = current;
              neighbor.setState(current.goal, current.current);
              neighbor.addAction(planner.getAction(neighbor.id));
              neighbor.g_score = g_score;
              neighbor.f_score = g_score + neighbor.distanceTo(goal)
              
              openset.push(neighbor);
            }
            
          }
        }
        
        return false;
      }
      
    };
        
    //////////////////////////////////////////
    // Action
    //////////////////////////////////////////
    var Action = GOAP.Action = function(name, spec) {
      this.name       = name;
      this.conditions = new State(spec.conditions);
      this.effects    = new State(spec.effects);
    };
    
    //////////////////////////////////////////
    // Graph
    //////////////////////////////////////////
    var Graph = GOAP.Graph = function(actionset) {
      this.nodes       = {};
      this.key_effects = {};
      
      // Build the initial graph; no edges
      for (var name in actionset) {
        this.nodes[name] = new Action(name, actionset[name]);
      }
      
      // Build the key-effects table
      for (var name in this.nodes) {
        this.nodes[name].effects.forEachKey(function(key, value, keys) {
          if (this.key_effects[key] === undefined) this.key_effects[key] = [];
          this.key_effects[key].push(name);
        }, this);
      }
    };
    
    Graph.prototype = {
      getNeighbors: function(keys) {
        var neighbors = [];
        
        for (var key in keys) {
          if (this.key_effects[key]) neighbors.concat(this.key_effects[key]);
        }
        
        return neighbors;
      },
      
      getAction: function(id) {
        return this.nodes[i];
      },

      getActions: function() {
        return this.nodes;
      }
    };
    
    return GOAP;
}));
