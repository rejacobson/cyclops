(function() {

  QUnit.testStart(function() {
    GOAP.initialize(window.Actionset);
  });

  module('GOAP');

  test('#initialize -- Initialize with a global actionset -- GOAP.initialize(actionset)', function(){
    strictEqual(GOAP.getActionset(), window.Actionset, 'Global Actionset is initialized.');
  });

  test('#getGraph -- Caches a list of actionset graphs -- GOAP.getGraph(subset)', function(){
    var subset = Object.keys(window.Actionset).slice(0, 3);
    var graph = GOAP.getGraph(subset);
    var actions = graph.getActions();
    var keys = Object.keys(actions);

    strictEqual(keys.length, 3, 'The subset is a list of action names.');
    deepEqual(keys, subset, 'Has a subset of the global actions.');
    deepEqual(graph, GOAP.getGraph(subset), 'Subsequent queries return the cached graph.');
  });

  module('State');

  test('#new -- Create with an initial state. -- new GOAP.State({...})', function(){
    var state = new GOAP.State({a: 'A', b: 'B'});
    deepEqual(state.keys, {a: 'A', b: 'B'}, 'Keys match.');
  });

  test('#forEachKey -- Executes a callback function on each key/value in the state -- a.forEachKey(callback)', function() {
    var state = new GOAP.State({a:'A', b:'B', c:undefined});
    var expected = {};
    state.forEachKey(function(value, key) {
      expected[key] = value;
    });

    deepEqual(expected, {a:'A', b:'B'}, 'Keys run in the callback match the keys in the state');
  });

  test('#forEachKey -- Stops iteration if the callback returns a value', function() {
    var state = new GOAP.State({a:'A', b:'B', c:'C', d:'D'});
    var expected;
    var return_val;

    expected = {};
    return_val = state.forEachKey(function(value, key) { expected[key] = value; return true; });
    deepEqual(expected, {a:'A'}, 'Returned early and only iterated once.');
    deepEqual(return_val, true, 'Returned value is `true`.');

    expected = {};
    return_val = state.forEachKey(function(value, key) { expected[key] = value; return false; });
    deepEqual(expected, {a:'A'}, 'Returned early and only iterated once.');
    deepEqual(return_val, false, 'Returned value is `false`.');

    expected = {};
    return_val = state.forEachKey(function(value, key) { expected[key] = value; return undefined; });
    deepEqual(expected, {a:'A', b:'B', c:'C', d:'D'}, 'Did not return early.');
    deepEqual(return_val, undefined, 'Returned value is `undefined`.');
  });

  test('#clone -- Creates and returns copy of the State -- a.clone()', function() {
    var state = new GOAP.State({a:'A', b:'B'});
    var clone = state.clone();

    deepEqual(state.keys, clone.keys, 'Keys match.');
    notStrictEqual(state, clone, 'The state and clone are different objects.');
  });

  test('#distanceTo -- Returns the number of keys in `b` that are not in `a` -- a.distanceTo(b)', function() {
    var a = new GOAP.State({a:'A', b:'B', c:'C'});
    var b = new GOAP.State({c:'C', d:'D'});

    equal(a.distanceTo(b), 1, 'Distance from a to b is 2');
    equal(b.distanceTo(a), 2, 'Distance from b to a is 1');
  });

  test('#difference -- Return a new State with keys in `a` that are not in `b` -- a.difference(b);', function() {
    var a = new GOAP.State({a:'A', b:'B', c:'C'});
    var b = new GOAP.State({c:'C', d:'D'});  
    var c = new GOAP.State({a:'Z', b:'X', c:'C'});  
    var diffa = a.difference(b);
    var diffb = b.difference(a);
    var diffc = a.difference(c);

    deepEqual(diffa.keys, {a:'A', b:'B'}, 'a.difference(b)');
    deepEqual(diffb.keys, {d:'D'}, 'b.difference(a)'); 
    deepEqual(diffc.keys, {a:'A', b:'B'}, 'a.difference(c)');
  });

  test('#union -- Return a new State with keys in `a` combined with keys in `b` -- a.union(b);', function() {
    var a = new GOAP.State({a:'A', b:'B', c:'C'});
    var b = new GOAP.State({c:'C', d:'D'});  
    var c = new GOAP.State({a:'Z', b:'X', c:'C'});  
    var diffa = a.union(b);
    var diffb = b.union(a);
    var diffc = a.union(c);

    deepEqual(diffa.keys, {a:'A', b:'B', c:'C', d:'D'}, 'a.union(b)');
    deepEqual(diffb.keys, {c:'C', d:'D', a:'A', b:'B'}, 'b.union(a)'); 
    deepEqual(diffc.keys, {a:'Z', b:'X', c:'C'}, 'a.union(c)');
  });
  
  
  module('Set');

  test('', function() {
    
  });  

})();
