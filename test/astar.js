var State = function(state) {
  this.state = state;
};

State.prototype = {
  function heuristic(current_state) {
    return current_state.countMisses(this);
  },

  forEachKey: function(callback, context) {
    context = context || this;
    for (var key in this.state) {
      if (this.state[key] !== undefined) callback.call(context, key, this.state[key]);
    }
  },

  containsAll: function(state) {
    for (var key in state) {
      if (this.state[key] !== state[key]) return false;
    }
    return true;
  },

  containsSome: function(state) {
    for (var key in state) {
      if (this.state[key] === state[key]) return true;
    }
    return false;
  },

  countMatches: function(state) {
    matches = 0;
    state.forEachKey(function(key, value) {
      if (value === this.state[key]) matches++;
    } this);
    return matches;
  },

  countMisses: function(state) {
    misses = 0;
    state.forEachKey(function(key, value) {
      if (value !== this.state[key]) misses++;
    } this);
    return misses;
  },

  add: function(state) {
    for (var key in state) {
      if (state[key] !== undefined) this.state[key] = state[key];
    }
    return this;
  }
};

State.keys = [
  'isHealthLow',
  'hasTarget',
  'isSleeping',
  'isHungry',
  'isTargetDead',
  'isTargetVisible',
  'inMeleeRange',
  'hasVisibleEntities'
];





var PlannerMap = function(action_set) {
  var ActionNode = function(name, spec) {
    this.name = name;
    this.conditions = new State(spec.conditions);
    this.effects = new State(spec.conditions);
    this.func = spec.func;

    this.edges = [];

    this.addEdges = function(nodes) {
      this.edges.concat(nodes);
    }
  };
 
  this.nodes = {};
  this.key_effects = {};

  for (var key in State.keys) {
    this.key_effects[key] = [];   // list of Actions that contain this symbol in their effects state
  }

  for (var name in action_set) {
    this.nodes[name] = new ActionNode(name, action_set[name]);
  }

  var node;

  // Build the key effects table
  for (var action_name in this.nodes) {
    node = this.nodes[action_name];

    node.effects.forEachKey(function(key, value) {
      this.key_effects[key].push(action_name);
    });
  }

  // Connect the nodes with edges
  for (var action_name in this.nodes) {
    node = this.nodes[action_name];

    node.conditions.forEachKey(function(key, value) {
      node.addEdges(this.key_effects[key]);
    });
  }
};

AStarPlannerMap.prototype = {
  getPlannerNode: function(name) {
    var node = this.nodes[name], planner_node = new PlannerNode(name);
    planner_node.goal_state = node.conditions;
    planner_node.current_state = node.effects;
    return planner_node;
  },
 
  getNode: function(name) {
    return this.nodes[name];
  },
 
  getNeighbors: function(name) {
    return this.nodes[name].edges;
  }
};



var PlannerNode = function(id) {
  this.id = id;
  this.parent;
  
  this.f;
  this.g;
  this.h;
 
  this.goal_state;
  this.current_state;
};

PlannerNode.prototype = {
  distanceTo: function(node) {
    return this.goal_state.containsAll(node.current_state) ? 2 : 3;
  }
};

var PlannerStorage = function() {
  this.open_list = [];
  this.closed_list = [];
 
  // Open List
  this.addToOpenList = function(node) {
    this.open_list.push(node);
  }
 
  this.isInOpenList = function(node) {
    for (var i in this.open_list) {
      if (node == this.open_list[i]) return true;
    }
  }
 
  this.getCheapestOpenNode = function() {
    return this.open_list.shift();
  }
 
  this.removeFromOpenList = function(node) {
    for (var i in this.open_list) {
      if (node == this.open_list[i]) {
        this.open_list.splice(i, 1);
        break;
      }
    }
  }
 
  // Closed List
  this.addToClosedList = function(node) {
    this.closed_list.push(node);
  }
 
  this.isInClosedList = function(node) {
    for (var i in this.closed_list) {
      if (node == this.closed_list[i]) return true;
    }
  }
 
  this.removeFromClosedList = function(node) {
    for (var i in this.closed_list) {
      if (node == this.closed_list[i]) {
        this.closed_list.splice(i, 1);
        break;
      }
    }
  }
};




function AStar = function(map, goal_state, current_state) {
  var storage = new PlannerStorage(),
      current_node;
  
  var node;
  for (var action_name in map.nodes) {
    node = map.nodes[action_name];
    if (node.effects.containsSome(goal_state)) storage.addToOpenList(map.getPlannerNode(action_name));
  }
  
  var h = goal_state.heuristic(current_state);
  current_node.g = 0;
  current_node.f = h;
  current_node.h = h;
  
  var neighbors, neighbor;
  while (storage.open_list.length) {
    current_node = storage.getCheapestOpenNode();
    storage.addToClosedList(current_node);
    if (current_node.goal_state == current_state) {
      return reconstruct_path();
    }
    
    neighbors = map.getNeighbors(current_node.id);
    
    for (var i in neighbors) {
      neighbor = neighbors[i];
      
      if (storage.isInOpenList(neighbor)) {
        
      }
    }
  }
}




