var State = function(state) {
  this.state = state;
};

State.prototype = {
  forEachKey: function(callback, context) {
    context = context || this;
    for (var key in this.state) {
      if (this.state[key] !== undefined) callback.call(context, key, this.state[key]);
    }
  },
  
  containsAll: function(state) {
    for (var key in state) {
      if (this.state[key] !== state[key]) return false;
    }
    return true;
  },
  
  containsSome: function(state) {
    for (var key in state) {
      if (this.state[key] === state[key]) return true;
    }
    return false;
  },
  
  countMatches: function(state) {
    matches = 0;
    state.forEachKey(function(key, value) {
      if (value === this.state[key]) matches++;
    } this);
    return matches;
  },
  
  countMisses: function(state) {
    misses = 0;
    state.forEachKey(function(key, value) {
      if (value !== this.state[key]) misses++;
    } this);
    return misses;
  },
  
  add: function(state) {
    for (var key in state) {
      if (state[key] !== undefined) this.state[key] = state[key];
    }
    return this;
  }
};

State.keys = [
  'isHealthLow',
  'hasTarget',
  'isSleeping',
  'isHungry',
  'isTargetDead',
  'isTargetVisible',
  'inMeleeRange',
  'hasVisibleEntities'
];


var Goal = function(state_spec, relevance_callback) {
  this.state = new State(state_spec);
  this.plan;
 
  this.select = function(action_set) {
    return this.plan = Planner.build(this, action_set);
  }
 
  this.update = function(entity) {
    this.plan.execute(entity);
  }
 
  this.relevance = function(entity) {
    return relevance_callback.call(entity);
  }
 
  this.isSatisfied = function(current_state) {
    return current_state.containsAll(this.state);
  }
  
  this.isPartiallySatisfied = function(current_state) {
    return current_state.containsSome(this.state);
  }
};

var Planner = (function() {
  var ActionNode = function(name, spec) {
    this.name = name;
    this.conditions = new State(spec.conditions);
    this.effects = new State(spec.conditions);
    this.func = spec.func;
    
    this.edges = [];
    
    this.addEdges = function(nodes) {
      this.edges.concat(nodes);
    }
  };
  
  return {
    build: function(goal, action_set) {
      var nodes = this.buildGraph(action_set);
      
      // Find a set of nodes that satisfy the current state
      var open_set = [];
      var current_node;
      for (var i in nodes) {
        node = nodes[i];
        if (node.effects.partialMatch(goal)) open_set.push(node);
      }
      
      while (open_set.length) {
        current_node = open_set.pop();
        
      }
    },
    
    buildGraph: function(action_set) {
      var nodes = {};
      var key_effects = {};
      
      for (var key in State.symbols) {
        symbol_effects[key] = [];   // list of Actions that contain this symbol in their effects state
      }
      
      for (var name in action_set) {
        nodes[name] = new ActionNode(name, action_set[name]);
      }
      
      var node;
      
      // Build the key effects table
      for (var action_name in nodes) {
        node = nodes[action_name];
        
        node.effects.forEachKey(function(key, value) {
          symbol_effects[key].push(action_name);
        });
      }
      
      // Connect the nodes with edges
      for (var action_name in nodes) {
        node = nodes[action_name];
        
        node.conditions.forEachKey(function(key, value) {
          node.addEdges(key_effects[key].map(function(aname){ return nodes[aname]; }));
        });
      }
      
      // Now we have a fully connected graph of nodes
      return nodes;
    },
    
    findPath: function(nodes, current_state, goal_state) {
      
    },
    
    aStar: function(nodes, current_state, goal_state) {
      function in_set(test, set) {
        for (var i in set) {
          if (set[i] == test) return true;
        }
      }
      
      function dist_between(node_a, node_b) {
        return node_a.conditions.containsAll(node_b.effects) ? 1 : 2;
      }
      
      function heuristic(s_state, e_state) {
        return e_state.countMisses(s_state) * 3;
      }
      
      function reconstruct_path(path, current) {
        
      }
      
      // Begin at the goal state and try to find a path ending up at the current state
      var start_state = goal_state,
          end_state = current_state,
          start_node,
          node;
          
      // Find candidate starting nodes having an effect that matches the goal_state
      var open_set = [],
          closed_set = [],
          came_from = {},
          current_node;
      
      for (var action_name in nodes) {
        node = nodes[action_name];
        if (node.effects.containsSome(start_state)) open_set.push(node);
      }
      
      open_set.sort(function(a, b) {
        var ascore = a.effects.containsAll(a) ? 1 : 2,
            bscore = b.effects.containsAll(b) ? 1 : 2;
        return ascore - bscore;
      });
      
      start_node = open_set[0];
      open_set = [start_node];
      
      var g_score = {},
          f_score = {},
          tentative_g_score;
      
      g_score[start_node.name] = 0;
      f_score[start_node.name] = heuristic(start_state, end_state);
      
      while (open_set.length) {
        current_node = open_set[0];
        if (current_node.effects.containsAll(end_state)) {
          return reconstruct_path(came_from, end_state);
        }
        
        open_set.shift();
        
        closed_set.push(current_node);
        
        var neighbor_node;
        for (var i in current_node.edges) {
          neighbor_node = current_node.edges[i];
          if (!g_score[neighbor_node.name]) g_score[neighbor_node.name] = 0;
          
          tentative_g_score = g_score[current_node.name] + dist_between(current_node, neighbor_node);
          
          if (in_set(neighbor_node, closed_set)) {
            if (tentative_g_score >= g_score[neighbor_node.name]) {
              continue;
            }
          }
          
          if (!in_set(neighbor_node, open_set) || tentative_g_score < g_score[neighbor_node.name]) {
            came_from[neighbor_node.name] = current_node;
            g_score[neighbor_node.name] = tentative_g_score;
            f_score[neighbor_node.name] = g_score[neighbor_node.name] + heuristic(neighbor_node.effects, end_state);
            if (!in_set(neighbor_node, open_set) {
              open_set.push(neighbor_node);
            }
          }
          
        } // end for (var i in current_node.edges)
        
      } // end while (open_set.length)
      
      return false;
    }
    
  };
  
})();
