define(['../lib/underscore', './server'], function(_, server) {
  
  var io = server.io;

  var Room = function(name, socket, hs) {
    this.name = name;
    this.game;

    var messages = [];

    // Get a list of rooms
    socket.on('room:list', function() {
      socket.emit('room:list', _.keys(io.sockets.manager.rooms)); 
    });

    // Get a list of players
    socket.on('room:players', function() {
      //socket.emit('room:players', _.keys(io.sockets.manager.rooms)); 
    });

    // Join/create a room
    socket.on('room:join', function(data) {
      socket.join(data);
      this.emit('room:join', 'playerId');
    });

    // leave a room
    socket.on('room:leave', function() {
      socket.leave();
      this.emit('room:leave', 'playerId');
    });

    socket.on('room:msg', function(data) {
      console.log('Messaging a room!!!!!!!');
      console.log(data);
    });
  };

  Room.prototype.emit = function(event_name, data) {
    io.sockets.in(this.name).emit(event_name, data);
  });

  return Room;

}); 
