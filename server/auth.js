define(function() {

  return {
    loggedIn: function(req, res, next) {
      if (req.session.account) {
        next();
      } else {
        res.json(401, {error: 'Not logged in'});
      }
    }
  };

});
