define(['module', 'path', 'connect', 'express', 'http', 'socket.io', 'ejs', './db', './routes', './auth'],
function(module, path, connect, express, http, socketio, ejs, db, routes, auth) {

  var SECRET = 'puppies-and-rainbows';

  var app = express(),
      server = http.createServer(app),
      io = socketio.listen(server),
      sessionStore = new connect.session.MemoryStore(),
      cookieParser = express.cookieParser(SECRET),
  
      // https://github.com/jrburke/requirejs/issues/107
      __filename = module.uri,
      __dirname = path.dirname(__filename),
      __dirname = __dirname.substr(0, __dirname.lastIndexOf('/'));

  app.configure(function() {
    app.set('views', __dirname + '/views');
    app.engine('html', ejs.renderFile);
    app.use('/', express.static(__dirname + '/public'));
    app.use(express.bodyParser());
    app.use(cookieParser);
    app.use(express.session({
        key: 'express.sid',
        secret: SECRET,
        store: sessionStore
    }));
  });

  io.set('authorization', function(data, accept) {
    if (!data.headers.cookie) {
      return accept('Session cookie required', false);
    }

    data.cookie = cookieParser(data, {}, function(err) {
      if (err) {
        return accept('COOKIE_PARSE_ERROR');
      }   
  
      var sessionID = data.signedCookies['express.sid'];

      data.sessionStore = sessionStore;

      sessionStore.load(sessionID, function(err, session) {
        if (err) {
          return accept('Error in session store', false);
        } else if (!session) {
          return accept('Session not found', false);
        }
        data.session = session;
        return accept(null, true);    
      });
    });
  });

  io.sockets.on('connection', function (socket) {
    var hs = socket.handshake;

    // setup an inteval that will keep our session fresh
    var intervalID = setInterval(function () {
        // reload the session (just in case something changed,
        // we don't want to override anything, but the age)
        // reloading will also ensure we keep an up2date copy
        // of the session with our connection.
        hs.session.reload( function () { 
            // "touch" it (resetting maxAge and lastAccess)
            // and save it back again.
            hs.session.touch().save();
        });
    }, 60 * 1000);

    socket.on('disconnect', function () {
        console.log('A socket with sessionID ' + hs.sessionID 
            + ' disconnected!');
        // clear the socket interval to stop refreshing the session
        clearInterval(intervalID);
    });

    socket.on('asdf', function(data) {
      hs.session.fart = 'booger';
      hs.session.save();
console.log('#asdf');
//console.log(hs.session);
console.log(data);
    });

    socket.on('qwerty', function (data) {
      hs.session.fart = 'ear wax';
      hs.session.save();
console.log('#qwerty');
//console.log(hs.session);
console.log(data);
    });

    socket.on('session', function(data) {
console.log('#session');
//console.log(hs.session);
console.log(hs.sessionID);
    });

    // 1. login/register/create character
    // 2. get list of rooms
    // 3. join/create a room
    // 4. Begin/continue game 

  });

  app.post('/api/signin', routes.signin);
  app.get('/api/signout', auth.loggedIn, routes.signout);

  app.get('/api/character', auth.loggedIn, routes.character.list);
  app.get('/api/character/:id', auth.loggedIn, routes.character.find);
  app.post('/api/character', auth.loggedIn, routes.character.create);
  app.get('/api/character/select', auth.loggedIn, function(){});

  app.get('/api/world', auth.loggedIn, routes.world.list);
  app.get('/api/world/:id', auth.loggedIn, routes.world.find);
  app.post('/api/world', auth.loggedIn, routes.world.create);
  app.post('/api/world/join', auth.loggedIn, routes.world.join);

  app.get('*', routes.index);

  server.listen(8080);

  return {
    app: app,
    server: server,
    io: io
  };
});

