define(['module', 'pg', 'sequelize', 'bcrypt-nodejs'], function(module, Postgres, Sequelize, bcrypt) {

  module.exports = {
    sequelize: Sequelize,
    pg: Postgres,
    postgres: Postgres,
    postgresql: Postgres
  };

  var sequelize = new Sequelize('cyclops', '', null, {
    host: 'localhost',
    port: 5432,
    logging: console.log,
    dialect: 'postgres',
    define: { timestamps: true, paranoid: true, underscored: true }
  });

  /**
   * Models
   */

  // Account
  var Account = module.exports.Account = sequelize.define('account', {
    email: Sequelize.STRING,
    encrypted_password: Sequelize.STRING
  }, {
    classMethods: {
      encryptPassword: function(plaintext) {
        return bcrypt.hashSync(plaintext); 
      }
    },
    instanceMethods: {
      authenticate: function(plaintext) {
        return bcrypt.compareSync(plaintext, this.encrypted_password);  
      }
    }
  });
  
  // Character
  var Character = module.exports.Character = sequelize.define('character', {
    name: Sequelize.STRING,
    job: Sequelize.STRING,
    str: Sequelize.INTEGER,
    int: Sequelize.INTEGER,
    dex: Sequelize.INTEGER
  });

  // World
  var World = module.exports.World = sequelize.define('world', {
    name: Sequelize.STRING,
    seed: Sequelize.STRING,
    players: Sequelize.INTEGER
  });

  Character.hasOne(Account);
  Character.hasOne(World);

  World.hasOne(Account);
  World.hasMany(Character);

  Account.hasMany(Character);
  Account.hasMany(World);

  sequelize.sync({force: true});

});
