define(['./db'], function(db) {

  return {
    index: function(req, res) {
      req.session.asdf = "booger";
      res.render('index.html');
    },

    // Login or Signup
    signin: function(req, res) {
      db.Account.find({ where: { email: req.body.email } }).success(function(account){

        // Found an account
        if (account) {
          
          // Password is good
          if (account.authenticate(req.body.password)) {
            req.session.account = account;
            res.json({success: 1, account_id: account.id, message: 'You have been logged in as, '+ req.body.email});
          } 

          // Bad password
          else {
            res.json({error: 'Wrong email address or password.'});
          }
        }

        // New account
        else {
          db.Account.create({email: req.body.email, encrypted_password: db.Account.encryptPassword(req.body.password)}).success(function(account){
            // login account
            req.session.account = account;
            res.json({success: 1, account_id: account.id, message: 'A new account has been created for, '+ req.body.email});
          }); 
        }

      });
    },

    signout: function(req, res) {
      req.session.account = null;
      res.redirect('/');
    },

    // Character list
    character: {
      find: function(req, res) {
console.log('Character #find');
console.log(req.params);
        db.Character.find({where: {id: req.params.id, account_id: req.session.account.id}}).success(function(character) {
          res.json(character);
        });
      },

      list: function(req, res) {
console.log('Character #list');
        db.Character.findAll({where: {account_id: req.session.account.id}}).success(function(characters) {
          res.json(characters);
        });
      },

      create: function(req, res) {
console.log('Character #create');

        req.body.account_id = req.session.account.id;
        db.Character.create(req.body).success(function(character) {
          if (character) {
            res.json(character.values);
          } else {
            res.json({error: 'Could not create a new Character'});
          }
        });
      }
    },

    world: {
      find: function(req, res) {
console.log('World #find');
console.log(req.body);
console.log(req.params);
        db.World.find({where: {id: req.params.id}}).success(function(world) {
          res.json(world);
        });
      },

      list: function(req, res) {
console.log('World #list');
console.log(req.body);
        var params = Object.keys(req.body).length > 0 ? req.body : ['players > ? OR account_id = ?', 0, req.session.account.id];
        db.World.findAll({where: params}).success(function(worlds) {
          res.json(worlds);
        });
      },

      create: function(req, res) {
        req.body.account_id = req.session.account.id;
        db.World.create(req.body).success(function(world) {
          if (world) {
            res.json(world.values);
          } else {
            res.json({error: 'Could not create a new World'});
          }
        });
      },

      join: function(req, res) {

      }
    }
  };

});
