var requirejs = require('requirejs');

requirejs.config({
  paths: {
    'underscore': 'public/js/game/lib/underscore'
  },
  nodeRequire: require
});


requirejs(['server/server', 'public/js/game/game'], function(server, Game) {
  // Setup game world

  //var game = new Game();
  //game.newArena();

/*
  io.sockets.on('connection', function(socket) {
    socket.emit('news', { hello: 'world' });
    socket.on('my other event', function (data) {
      console.log(data);
    });
  });
*/

/*
  io.sockets.on('connection', function(socket) {
    var playerid = null;
    
    // Player joins the game
    socket.on('join', function(data) {
      if (game.playerExists(data.name)) {
        // Don't allow duplicate names.
        return;
      }
      if (game.getPlayerCount() >= 4) {
        // Don't allow more than 4 players.
        return;
      }
      playerId = game.join(data.name);
      data.timeStamp = new Date();
      
      socket.broadcast.emit('join', data);
      data.isme = true;
      socket.emit('join', data);
    });
    
    // Player requests a state dump
    socket.on('state', function(data) {
      socket.emit('state', {
        state: game.save()
      });
    });

    // Player performs an action  
    socket.on('action', function(data) {
      game.action(playerid, data);
      // If successful, broadcast to other players
      socket.broadcast.emit('action', data);
    });
    
    // Player disconnects
    socket.on('disconnect', function(data) {
      
    });
  */
    
//  });



  /*
  Chat server
  var buffer = [];
  io.on('connection', function(client){
      client.send({ buffer: buffer });
      client.broadcast({ announcement: client.sessionId + ' connected' });

      client.on('message', function(message){
          var msg = { message: [client.sessionId, message] };
          buffer.push(msg);
          if (buffer.length > 15) buffer.shift();
          client.broadcast(msg);
      });

      client.on('disconnect', function(){
          client.broadcast({ announcement: client.sessionId + ' disconnected' });
      });
  });
  */

});
