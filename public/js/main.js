require.config({
  paths: {
    'angular':          'app/lib/angular/angular',
    'angular-cookie':   'app/lib/angular/cookie',
    'angular-resource': 'app/lib/angular/angular-resource.min',
    'jquery-ui':        'app/lib/jquery-ui-1.10.2.custom.min',
    'socket.io':        'app/lib/socket.io.min',
    'underscore':       'game/lib/underscore'
  },
  priority: [
    'angular'
  ]
});

require(['jquery', 'jquery-ui', 'angular', 'app/ng-app', 'app/ng-routes', 'app/ng-directives'], function($, jqueryui, angular, app, routes) {

  $(function() {
    // Bootstrap Angular.js
console.log('jQuery has loaded');
    var $html = $('html');
console.log('Bootstrapping angular');
console.log(app['name']);
console.log(app);
    $html.addClass('ng-app');
    angular.bootstrap($html, [app['name']]);
console.log('Angular has been bootstrapped.');

    console.log('Initialized the project with jQuery');


/*
    var multiplayer = false;

    var game = new Game();
    var player = new Player();
    var renderer = new HtmlRenderer();

    game.join(player);


    if (multiplayer) {
      /**
        var client = new Client(game)

        1. Login
           - client.sessionid(data.sessionid)
        
        2. Select/Create Character
           - get character list
           - pick/create character
           - player.use(character);

        3. Join/Create Room/Game
           - game.load(gamedata.world);
           - player.id(gamedata.playerid);
           - game.join(player);
           - for (i in gamedata.players) game.join(gamedata.players[i])

        4. Enter Area

      * /

/*
      $.get(Client.server_uri, function() {
        Client.initialize(game); 
      });

      socket.on('character.select', function(data) {
        var creature = new Creature(data);
        player.use(creature);
      });
* /
    } else {
      var creature = new Creature('player', 'Ryan');
      player.use(creature);
      var input = new DesktopInput(game, player.avatar);

      // Area generation
      // 1. Terrain generation
      // 2. Static Entities for a maze, forest, dungeon, castle, swamp, etc. -- Trees, walls, chasms
      // 3. Creature selection and placement
      // 4. Item selection and placement
      var terrain = new TerrainGenerator();
      var area = new Area(terrain);
      game.setArea(area);
      area.insertEntity(player.avatar);

      var mouse = new Creature('mouse');
      mouse.setPosition(5, 5);
      mouse.simulate = function() {
        mouse.perform('move', 2);
      };

      area.insertEntity(mouse);
      //AI.control('scaredy-cat', mouse);

      Event.on('player.performed', function() {
        if (area.getReadyPlayers().length <= 0) area.tick();
      });

      Event.on('area.ticked', function() {
      });
    
      renderer.renderArea(area);
    }
*/

  });

});
