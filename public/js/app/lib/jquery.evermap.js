// Uses AMD or browser globals to create a jQuery plugin.

// It does not try to register in a CommonJS environment since
// jQuery is not likely to run in those environments.
// See jqueryPluginCommonJs.js for that version.

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

  $.fn.evermap = function(options) {
    options = $.extend({
      mapElement: 'ul',
      tileElement: 'li',
      updateHandler: null
    }, options || {});
        
    this.each(function() {
      $(this).data('evermap', new Evermap(this, options));
    });

    return this;
  };

  var _ids = 0;
  
  var Evermap = function(viewport, options) {
    this.options = options;
    this.id      = ++_ids;
    
    this.$viewport = $(viewport);
    this.$map      = $('<'+ options.mapElement +'>').appendTo(this.$viewport);
    this.$tiles;
    
    this.tilesize = [0, 0];
    this.gridsize = [0, 0];
    this.topleft  = [0, 0];
    
    // Calculate the tile size
    var tile = this._makeTile().hide().appendTo(this.$map);
    this.tilesize = [tile.width(), tile.height()];
    tile.remove();
    
    // Calculate the number of tiles spanning the width and height of the viewport
    this.gridsize[0] = Math.ceil(this.$viewport.width() / this.tilesize[0]) + 2;
    this.gridsize[1] = Math.ceil(this.$viewport.height() / this.tilesize[1]) + 2;
    
    // Create the tiles in the map
    for (var x=0, xlen=this.gridsize[0]; x<xlen; ++x) {
      for (var y=0, ylen=this.gridsize[1]; y<ylen; ++y) {
        tile = this._makeTile().css({left: x * this.tilesize[0], top: y * this.tilesize[1]});
        tile[0].x = x;
        tile[0].y = y; 
        tile[0].id = this._makeId(x, y);
        tile.appendTo(this.$map);
      }
    }
    this.$tiles = $.makeArray($('> '+ this.options.tileElement, this.$map));
    
    // Center the map
    this.$map.css({left: -this.tilesize[0], top: -this.tilesize[1]});
    
    // Setup the draggable
    var self = this;

    this.$map.draggable({
      start: function(event, ui) {
        this.last_position = ui.position;
      },
      
      drag: function(event, ui) {
        self._calculateNewTilePositions(this.last_position, ui.position);
        this.last_position = ui.position;
      },

      stop: function(event, ui) {
        self._calculateNewTilePositions(this.last_position, ui.position);
      }
    });

  };
  
  Evermap.prototype = {
    /**
     * Public
     */
    getTile: function(x, y) {
      return $('#'+ this._makeId(x, y))[0];
    },

    getTiles: function(coords) {
      if (arguments.length == 0) return this.$tiles;
      if (!(coords instanceof Array)) return this.getTile(coords);

      var tiles = [], x, y;
      for (var i=0, len=coords.length; i<len; ++i) {
        x = coords[i][0];
        y = coords[i][1];
        if (this.outOfBounds(x, y)) continue;
        tiles.push(this.getTile(x, y)); 
      }
      return tiles;
    },

    getRow: function(y) {
      var ids = [];
      for (var x=this.topleft[0], len=this.topleft[0] + this.gridsize[0]; x<len; ++x) {
        ids.push('#'+ this._makeId(x, y));
      }
      return $(ids.join(','));
    },
    
    getCol: function(x) {
      var ids = [];
      for (var y=this.topleft[1], len=this.topleft[1] + this.gridsize[1]; y<len; ++y) {
        ids.push('#'+ this._makeId(x, y));
      }
      return $(ids.join(','));
    },
    
    outOfBounds: function(x, y) {
      return x < this.topleft[0] ||
             x > (this.topleft[0] + this.gridsize[0]) || 
             y < this.topleft[1] ||
             y > (this.topleft[1] + this.gridsize[1]);
    },

    getAbsPosition: function(x, y) {
      return [x * this.tilesize[0], y * this.tilesize[1]];
    },

    refresh: function(coords) {
      var tiles = (coords instanceof Array && coords.length) ? this.getTiles(coords) : this.$tiles;
      this._updateTiles(tiles);
    },
    
    /**
     * Private
     */
    _makeId: function(x, y) {
      return '_'+ this.id +'_'+ x +'_'+ y;
    },
    
    _makeTile: function() {
      return $('<'+ this.options.tileElement +'>');
    },
    
    _getTopLeftTile: function() {
      return $('#'+ this._makeId(this.topleft[0], this.topleft[1]));
    },
    
    _flipRight: function() {
      var tiles = this.getCol(this.topleft[0]),
          left = (parseInt(tiles[0].style.left) + this.gridsize[0] * this.tilesize[0]) +'px',
          x = tiles[0].x + this.gridsize[0];
      
      for (var i=0, len=tiles.length; i<len; ++i) {
        tiles[i].x = x;
        tiles[i].style.left = left;
      };
      
      this.topleft[0] += 1;
      
      this._updateTiles(tiles);
    },
    
    _flipLeft: function() {
      var tiles = this.getCol(this.topleft[0] + this.gridsize[0] - 1),
          left = (parseInt(tiles[0].style.left) - this.gridsize[0] * this.tilesize[0]) +'px',
          x = tiles[0].x - this.gridsize[0];
      
      for (var i=0, len=tiles.length; i<len; ++i) {
        tiles[i].x = x;
        tiles[i].style.left = left;
      };
      
      this.topleft[0] -= 1;
      
      this._updateTiles(tiles);
    },
    
    _flipBottom: function() {
      var tiles = this.getRow(this.topleft[1]),
          top = (parseInt(tiles[0].style.top) + this.gridsize[1] * this.tilesize[1]) +'px',
          y = tiles[0].y + this.gridsize[1];
      
      for (var i=0, len=tiles.length; i<len; ++i) {
        tiles[i].y = y;
        tiles[i].style.top = top;
      };
      
      this.topleft[1] += 1;
      
      this._updateTiles(tiles);
    },
    
    _flipTop: function() {
      var tiles = this.getRow(this.topleft[1] + this.gridsize[1] - 1),
          top = (parseInt(tiles.css('top')) - this.gridsize[1] * this.tilesize[1]) +'px',
          y = tiles[0].y - this.gridsize[1];
      
      for (var i=0, len=tiles.length; i<len; ++i) {
        tiles[i].y = y;
        tiles[i].style.top = top;
      };
      
      this.topleft[1] -= 1;
      
      this._updateTiles(tiles);
    },

    _updateTiles: function(tiles) {
      var evermap = this;

      for (var i=0, len=tiles.length; i<len; ++i) {
        tiles[i].id = this._makeId(tiles[i].x, tiles[i].y);
      };
      
      if (this.options.updateHandler) {
        setTimeout(function() {
          evermap.options.updateHandler.call(evermap, tiles);
        }, 0);
      }
    },

    _calculateNewTilePositions: function(last_position, position) {
      var $maintile = this._getTopLeftTile(),
          left_pos = parseInt($maintile[0].style.left) + parseInt(this.$map[0].style.left) + this.tilesize[0],
          top_pos  = parseInt($maintile[0].style.top) + parseInt(this.$map[0].style.top) + this.tilesize[1];

      // Dragging left
      if (last_position.left > position.left) {
        while (left_pos < 0) {
          this._flipRight();
          left_pos += this.tilesize[0];
        }
      } 
      // Dragging Right
      else if (last_position.left < position.left) {
        while (left_pos > 0) {
          this._flipLeft();
          left_pos -= this.tilesize[0];
        }
      }
    
      // Dragging Up
      if (last_position.top > position.top) {
        while (top_pos < 0) {
          this._flipBottom();
          top_pos += this.tilesize[1];
        }
      }
      
      // Dragging Down
      else if (last_position.top < position.top) {
        while (top_pos > 0) {
          this._flipTop();
          top_pos -= this.tilesize[1];
        }
      }
    }

  };

}));
