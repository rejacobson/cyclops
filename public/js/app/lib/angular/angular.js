define(['app/lib/angular/angular.min'], function (angular) {

  if (typeof _ != 'undefined') {
    _.noConflict();
  }

  if (typeof $ != 'undefined') {
    $.noConflict();
  }

  return window.angular;

});
