define(['./ng-app'], function(app) {

 return app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
      $routeProvider.
        when('/', {
          templateUrl: '/views/index.html',
          controller: 'homeCtrl'
        }).
        when('/character/list', {
          templateUrl: '/views/character/list.html',
          controller: 'characterListCtrl'
        }).
        when('/character/create', {
          templateUrl: '/views/character/create.html',
          controller: 'characterCreateCtrl'
        }).
        when('/world/list', {
          templateUrl: '/views/world/list.html',
          controller: 'worldListCtrl'
        }).
        when('/world/create', {
          templateUrl: '/views/world/create.html',
          controller: 'worldCreateCtrl'
        }).
        when('/game', {
          templateUrl: '/views/game/index.html',
          controller: 'gameCtrl'
        }).
        when('/demo', {
          templateUrl: '/views/demo/index.html',
          controller: 'demoCtrl'
        });

      $locationProvider.html5Mode(true);
    }]); /*.

    run(['$restoregamestate', function($restoregamestate) {
      $restoregamestate();
    }]);*/

});
