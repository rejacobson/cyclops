define(['angular'], function(angular) {

  return angular.module('MyApp.modules', []).
    factory('$restoregamestate', function(GameState, CharacterService, WorldService) {
      return function() {
        CharacterService.use(GameState.get('character'));
        WorldService.use(GameState.get('world'));
      };
    }).

    factory('$game', function() {
      var Game = new Game();

    });

});
