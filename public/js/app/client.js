// https://github.com/borismus/osmus
define(['jquery', 'socket.io', 'game/event'], function($, io, Event) {

  var Client = function(game) {
    this.game = game;
    this.socket = io.connect(Client.server_uri);
    this.sessionid;

    var self = this;

    // Local events
    Event.on('player.performed', function() {
      self.emit('player.performed', { action: arguments[0], params: arguments.slice(1) });
    }); 

    // Server socket events
    this.socket.on('chat.message', function(data) {

    });

    this.socket.on('player.performed', function(data) {
      this.game.getPlayer(data.id).perform(data.action, data.params);
    });

    this.socket.on('area.ticked', function(data) {
      this.area.update(data);
      this.area.tickPlayers();
    });
  };

  Client.prototype = {
    emit: function(event_name, data) {
      data.id = this.sessionid;
      this.socket.emit(event_name, data); 
    }    
  };

  Client.server_uri = 'http://localhost:5050';

  return new Client();

});
