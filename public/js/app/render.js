define(['jquery', 'underscore', 'game/tile'], function($, _, Tile) {

  var Renderer = function(game) {
    this.game = game;
    this.$arena = $('#arena');
  };

/*
  Renderer.prototype.renderWorldMap = function() {

  };
*/

  Renderer.prototype.renderArena = function() {
    var arena = this.game.getArena();

    // Render tiles
    for (var i=0, len=arena.tiles.length; i<len; ++i) {
      $('#tile_'+ i).remove();

      var y = Math.floor(i / arena.width),
          x = i - (y * arena.width),
          tile = this.renderTile(arena.tiles[i]);

      tile.css({left: x * Tile.size[0], top: y * Tile.size[1]}).attr('id', 'tile_'+ i);
      this.$arena.append(tile);
    }

    // Render items
    // Render entities
    // Render players
  };

  Renderer.prototype.renderTile = function(name) {
    var tile = Tile.info(name);
    return $('<img>').addClass('tile').attr({src: tile.image}).css({width: Tile.size[0], height: Tile.size[1]});
  };

  return Renderer;

});
