define(function() {

  return {
    // Terrain
    grass:  { char: ',', color: '#00aa00' },
    stone:  { char: '.', color: '#999999' },
    mud:    { char: '~', color: '#402000' },
    water:  { char: '~', color: '#2266bb' },

    // Creatures
    player: { char: '@', color: '#ffffff' },
    mouse:  { char: 'm', color: '#ffffff' }
  };


});
