define(['jquery', './charmap', 'game/event'], function($, Charmap, Event) {

  var HtmlRenderer = function() {
    this.id = 'html-area';  

    var self = this;
    Event.on('area.ticked', function(area) {
      self.renderArea(area);
    }); 
  };

  HtmlRenderer.prototype = {
    _table: function(area) {
      var table = $('#'+ this.id);

      if (!table.length) {
        table = $('<table>')
          .attr('id', this.id)
          .css({
            'font-family':'Courier New',
            'border-collapse':'separate',
            'border-spacing':2
          });

        for (var y=0, leny=area.height(); y<leny; ++y) {
          var tr = $('<tr>');

          for (var x=0, lenx=area.width(); x<lenx; ++x) {
            var td = $('<td>')
              .attr('id', '_'+ x +'_'+ y)
              .css({
                'width':10,
                'height':10, 
                'text-align':'center'});

            tr.append(td);
          }

          table.append(tr);
        }

        $('#area').html(table);
      }

      return table;
    },

    clearArea: function() {
      this._table().remove();
    },

    renderArea: function(area) {
      this.renderTerrain(area);
      this.renderEntities(area);
      return this._table();
    },

    renderTerrain: function(area) {
      var table = this._table(area),
          pos, cmap;

      area.forEachTile(function(tile, index, x, y) {
        pos = '_'+ x +'_'+ y;
        cmap = Charmap[tile.state.terrain];
          
        $('#'+ pos).html(cmap.char).css('color', cmap.color);
      });
    },

    renderItems: function(area) {
      var table = this._table(area);

    },

    renderEntities: function(area) {
      var table = this._table(area),
          self = this,
          pos, cmap;

      area.forEachEntity(function(entity){
        pos = '_'+ entity.position.x +'_'+ entity.position.y;
        cmap = Charmap[entity.type];

        $('#'+ pos).html(cmap.char).css('color', cmap.color); 
      });  
    }
  };

  return HtmlRenderer;

});
