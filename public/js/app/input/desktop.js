define(['../lib/input'], function(Input) {

  var DesktopInput = function(player) {
    Input.on('keydown.kp2', function() { player.perform('move', 2); });
    Input.on('keydown.kp4', function() { player.perform('move', 4); });
    Input.on('keydown.kp6', function() { player.perform('move', 6); });
    Input.on('keydown.kp8', function() { player.perform('move', 8); });
    Input.on('keydown.kp1', function() { player.perform('move', 1); });
    Input.on('keydown.kp3', function() { player.perform('move', 3); });
    Input.on('keydown.kp7', function() { player.perform('move', 7); });
    Input.on('keydown.kp9', function() { player.perform('move', 9); });
  };

  return DesktopInput;

});
