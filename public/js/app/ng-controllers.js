define(['angular',
  './ng/controllers/home',
  './ng/controllers/account',
  './ng/controllers/character',
  './ng/controllers/world',
  './ng/controllers/game',
  './ng/controllers/demo'
], function(angular) { return angular; });
