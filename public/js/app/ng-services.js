define(['angular', 'angular-resource', 'angular-cookie'], function(angular, angularResource, angularCookie) {

  return angular.module('MyApp.services', ['ngResource', 'Cookie']).
    factory('GameState', function($resource, $cookie) {
      var gamestate = (function(){
        var _state = {};

        return {
          get: function(name) {
            if (!name) return _state;
            return _state[name];
          },

          set: function(name, value) {
            _state[name] = value;
            this.save();
            return value;
          },

          save: function() {
            $cookie('gamestate', JSON.stringify(_state), {path: '/', expiry: 7});
          },

          restore: function() {
            if (!$cookie('gamestate')) this.reset();
            _state = JSON.parse($cookie('gamestate'));
          },

          reset: function() {
            _state = {
              multiplayer: false,
              account: null,
              character: null,
              world: null  
            };
            this.save();
          }
        };
      })();

      gamestate.restore();

      return {
        singleplayer: function() {
          gamestate.reset();
          gamestate.set('multiplayer', false);
        },

        multiplayer: function() {
          gamestate.reset();
          gamestate.set('multiplayer', true);
        },

        set: function(name, value) {
          return gamestate.set(name, value); 
        },

        get: function(name) {
          return gamestate.get(name);
        }
      };
    }).

    factory('CharacterService', function($resource, GameState) {
      var Character = $resource('/api/character/:id', {id: '@id'});
      var current;

      return {
        current: function() {
          return current;
        },

        list: function() {
          // Multiplayer
          if (GameState.get('multiplayer')) {
            return Character.query();  
          }

          // Single Player 
          else {
            //return GameLocalStorage.get('characters');
          }
        },

        get: function(id, callback) {
          // Multiplayer
          if (GameState.get('multiplayer')) {
            return Character.get({id: id}, function(character, responseHeaders) {
              if (callback && angular.isFunction(callback)) callback(character);
            });  
          }

          // Single Player 
          else {
            /*var character = GameLocalStorage.get('characters')[id];
            return this.use(character);*/
          }
        },

        create: function(data) {
          var self = this, 
              character;

          // Multiplayer
          if (GameState.get('multiplayer')) {
            character = new Character(data); 
            return character.$save(function(c, postResponseHeaders) {
              self.use(character);   
            });
          }

          // Single Player
          else {
            /*character = new Character(data);
            GameLocalStorage.append('characters', character);
            return this.use(character);*/
          }  
        },

        use: function(character) {
          if (!isNaN(character)) {
            return this.get(character, function(c) {
              if (c.id) GameState.set('character', c.id);
              current = c;
            });
          }
          if (character instanceof Character) {
            GameState.set('character', character.id);
            current = character;
            return character;
          }
          return false;
        }
      };
    }).
  
    factory('WorldService', function($resource, GameState) {
      var World = $resource('/api/world/:id', {id: '@id'});
      var current;

      return {
        current: function() {
          return current;
        },

        list: function() {
          // Multiplayer
          if (GameState.get('multiplayer')) {
            return World.query();  
          }

          // Single Player 
          else {

          }
        },

        get: function(id, callback) {
          // Multiplayer
          if (GameState.get('multiplayer')) {
            return World.get({id: id}, function(world, responseHeaders) {
              if (callback && angular.isFunction(callback)) callback(world);
            });
          }

          // Single Player 
          else {

          }
        },

        create: function(data) {
          var self = this;

          // Multiplayer
          if (GameState.get('multiplayer')) {
            var world = new World(data); 
            return world.$save(function(w, postResponseHeaders) {
              self.use(w);
            });
          }

          // Single Player
          else {
            
          }
        },

        search: function(data) {
          if (GameState.get('multiplayer')) {
            return World.query(data);
          } else {

          }
        },

        use: function(world) {
          if (!isNaN(world)) {
            return this.get(world, function(w) {
              if (w.id) GameState.set('world', w.id);
              current = w;
            });
          }
          if (world instanceof World) {
            GameState.set('world', world.id);
            current = world;
            return world;
          }
          return false;
        }
      };
    });

});
