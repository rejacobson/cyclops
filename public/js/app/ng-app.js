define(['angular', 'angular-cookie', './ng-controller', './ng-controllers', './ng-services', './ng-modules'],
function(angulars) {

  return angular.module('MyApp', ['Cookie', 'MyApp.controllers', 'MyApp.services', 'MyApp.modules']);

});
