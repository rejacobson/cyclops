define(['jquery', 'app/ng-app', 'app/lib/jquery.evermap'], function($, app) {

  app.directive('areamap', function() {
    return {
      restrict: 'E',
      template: '<div class="areamap"></div>',
      link: function($scope, element, attrs, controller) {
        var areamap = element.find('.areamap');
        areamap.evermap({
          updateHandler: $scope.mapScroll
        });
        if ($scope.registerAreaMap) $scope.registerAreaMap(areamap.data('evermap'));
      }
    };
  });

});
