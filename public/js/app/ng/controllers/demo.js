define(['jquery', 'app/ng-controller', 'app/input/desktop', 'game/event', 'game/director', 'game/generator/area', 'game/entity/character/all', 'game/entity/creature'],
function($, controllerModule, DesktopInput, Event, Director, AreaGenerator, Characters, Creature) {

  var Renderer = function(area, map) {
    this.area = area;
    this.map = map;

    this.entitysprites = {};

    this.map.refresh();

    this.renderTiles = function(tiles) {
      if (!tiles || !tiles.length) return;

      if (tiles[0] instanceof Array) {
        tiles = this.map.getTiles(tiles); 
      }

      var area_tile, tile;
      for (var i=0, len=tiles.length; i<len; ++i) {
        tile = tiles[i];
        if (!area.tileExists(tile.x, tile.y)) {
          tile.className = '';
          continue;
        }
        tile.className = this.area.getTile(tile.x, tile.y).terrain().name;
      }
    }

    this._get_sprite = function(entity) {
      if (this.entitysprites[entity.id]) return this.entitysprites[entity.id]; 
      var sprite = $('<b>').addClass(entity.type).hide();
      this.map.$map.append(sprite);
      this.entitysprites[entity.id] = sprite;
      return sprite;
    }

    this._remove_sprite = function(entity) {
      if (this.entitysprites[entity.id]) {
        this.entitysprites[entity.id].remove();
        delete this.entitysprites[entity.id];
      }
    }

    this._place_sprite = function(entity) {
      var sprite = this._get_sprite(entity);
      if (this.map.outOfBounds(entity.position.x, entity.position.y)) return sprite.hide();
      var pos = this.map.getAbsPosition(entity.position.x, entity.position.y);
      sprite[0].style.left = (pos[0] + (this.map.tilesize[0]/2 - parseInt(sprite.width())/2)) +'px';
      sprite[0].style.top  = (pos[1] + (this.map.tilesize[1] - parseInt(sprite.height()))) +'px';  
      sprite.show();
    }

    this.renderEntities = function(entities) {
      if (!entities) return;
      if (!(entities instanceof Array)) entities = [entities];
      if (!entities.length) return;
      
      for (var i=0, len=entities.length; i<len; ++i) {
        this._place_sprite(entities[i]);  
      }
    }
  };

  return controllerModule.
    controller('demoCtrl', ['$scope', function($scope) {
      var area = AreaGenerator();
      var director = new Director(area);
      var renderer; 
      var player = new Characters.Soldier({name: 'ooog'});
      var rat = Creature.create('rat');

      player.setPosition(5, 2);

      DesktopInput(player);

      Event.on('character.performed', function() {
      });

      Event.on('entity.performed', function() {
      });

      Event.on('area.simulated', function() {
      });

      Event.on('entity.placed', function(entity) {
        renderer.renderEntities(entity);
      });

      $scope.registerAreaMap = function(map) {
        renderer = new Renderer(area, map);
        director.addPlayer(player);
        area.place(rat, 10, 10);
      };

      $scope.mapScroll = function($tiles) {
        renderer.renderTiles($tiles);
      }

      $scope.updateTiles = function(coords) {
        renderer.renderTiles(coords);
      }

    }]);

});


