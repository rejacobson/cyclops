define(['app/ng-controller'], function(controllerModule) {

  return controllerModule.
    controller('homeCtrl', function($scope, $location, GameState) {
console.log('This is home controller');
      $scope.singleplayer = function() {
        GameState.singleplayer();
        $location.path('/character/list');
      };
    });

});
