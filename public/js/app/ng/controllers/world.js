define(['app/ng-controller'], function(controllerModule) {

  return controllerModule.
    controller('worldListCtrl', function($scope, $location, WorldService, GameState) {
      if (!GameState.get('character')) return $location.path('/character/list');

      $scope.worlds = WorldService.list();

      $scope.select = function(id) {
        WorldService.use(id);
        $location.path('/world/play');
      };

      $scope.search = function() {
        $scope.worlds = WorldService.search({name: $scope.name});  
      };
    }).
  
    controller('worldCreateCtrl', function($scope, $location, WorldService) {
      $scope.submit = function() {
        WorldService.create($scope.world);
        $location.path('/world/play');
      };
    });

});

