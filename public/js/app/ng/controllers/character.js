define(['app/ng-controller'], function(controllerModule) {

  return controllerModule.
    controller('characterListCtrl', function($scope, $location, CharacterService) {
      $scope.characters = CharacterService.list();

      $scope.select = function(id) {
        if (CharacterService.use(id) !== false) {
          $location.path('/world/list');
        }
      };
    }).

    controller('characterCreateCtrl', function($scope, $location, CharacterService) {
      $scope.submit = function() {
        CharacterService.create($scope.char);
        $location.path('/character/list');
      };
    });

});
