define(['app/ng-controller'], function(controllerModule) {

  return controllerModule.
    controller('signinCtrl', function($scope, $http, $location, GameState) {
      $scope.submit = function() {
        $http.post('/api/signin', $scope.account).
          success(function(data, status, headers, config){
            if (data.success) {
              GameState.multiplayer();
              GameState.set('account', data.account_id);
              $location.path('/character/list');
            } else {

            }
          }).

          error(function(data, status) {
          });
      };
    });

});
