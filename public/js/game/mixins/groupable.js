define(['underscore'], function(_) {

  return {
    /**
     * this._group()              // return __groups object
     * this._group('name')        // return whether this group exists or not
     * this._group('name', false) // delete a group
     * this._group('name', true)  // add a group
     */
    _group: function(name, value) {
      if (!this.__groups) this.__groups = {};
      if (arguments.length == 0) return this.__groups;
      if (arguments.length == 1) return this.__groups[name];
      if (arguments.length == 2) {
        if (!value) return delete this.__groups[name];
        return this.__groups[name] = true;
      }
    },

    groups: function() {
      return _.keys(this._group());
    },

    joinGroup: function(name) {
      if (name instanceof Array) {
        for (var i in name) {
          this._group(name[i], true);
        }
      } else {
        this._group(name, true);
      }
    },

    leaveGroup: function(name) {
      this._group(name, false);
    },

    inGroup: function(name) {
      return !!this._group(name);
    },

    forEachGroup: function(callback) {
      var groups = this.groups();
      for (var i in groups) {
        callback.call(this, groups[i]);
      }
    }
    
  };

});
