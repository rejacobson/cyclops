define(['underscore'], function(_) {


  return {
    baseStats: function(basestats) {
      if (!this.__base_stats) this.__base_stats = {};
      if (basestats) {
        this.__base_stats = _.merge(this.__base_stats, basestats);
        var stats = this.stats();
        _.each(basestats, function(value, index) {
          if (!stats[index]) stats[index] = value; 
        });
      }
      return this.__base_stats;        
    },

    stats: function(stats) {
      if (!this.__stats) this.__stats = {};
      if (stats) {
        this.__stats = _.merge(this.__stats, stats);
        var basestats = this.baseStats();
        _.each(stats, function(value, index) {
          if (!basestats[index]) basestats[index] = value; 
        });
      }
      return this.__stats;        
    },

    baseStat: function(name, value) {
      var stats = this.baseStats();
      if (value !== undefined && stats[name] && !_.isFunction(stats[name])) stats[name] = value;
      return _.isFunction(stats[name]) ? stats[name].call(this) : stats[name];
    },

    stat: function(name, value) {
      var stats = this.stats();
      if (value !== undefined && stats[name] && !_.isFunction(stats[name])) stats[name] = value;
      return _.isFunction(stats[name]) ? stats[name].call(this) : stats[name];
    },

    hasStat: function(name) {
      return !!this.stats()[name];
    }

  };

});
