define(function() {

  return {
    state: function(state) {
      if (state) this._state = JSON.parse(JSON.stringify(state));
      if (!this._state) this._state = {};
      return this._state;
    },

    is: function(state, value) {
      if (value !== undefined) this.state()[state] = value;
      return !!this.state()[state];
    },

    get: function(state) {
      return this.state()[state];
    }
  };

});
