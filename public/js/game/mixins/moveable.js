define(function() {

  return {
    move: function(direction) {
      var x = this.position.x,
          y = this.position.y;

      switch (direction) {
        case 1: x-=1; y+=1; break;
        case 2: y+=1;       break;
        case 3: x+=1; y+=1; break;
        case 4: x-=1;       break;
        case 6: x+=1;       break;
        case 7: x-=1; y-=1; break;
        case 8: y-=1;       break;
        case 9: x+=1; y-=1; break;
      }

/*
      if (this.getArea().hasCreature(x, y)) {
        return this.perform('attack', [x, y]);
      }

      if (this.getArea().isBlocking(x, y)) return 0;
*/

      this._area.moveEntity([x, y], this); 

      return 1;
    }
  };

});

