define(function() {

  return {
    initTimer: function() {
      this.__timer = 1;  
    },

    setTime: function(t) {
      this.__timer = t;
    },
    
    addTime: function(t) {
      this.__timer += t;
    },
    
    useTime: function(t) {
      this.__timer -= t;
    },
    
    isReady: function() {
      return this.__timer > 0;
    }
  };

});
