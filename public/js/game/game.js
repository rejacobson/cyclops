define(['./event', './area'], function(Event, Area) {

  function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  var Game = function() {
    this.players = {};
    this.area;
    this.area_list = [];
  };

  Game.prototype._createArea = function(number) {
/*
    var area = new Area(25, 25);
    var available_tiles = Tile.names();
    var tiles = [];

    var index;
    area.traverseCoordinates(function(x, y) {
      index = y * area.width + x;
      tiles[index] = available_tiles[randomInt(0, available_tiles.length-1)];
    });

    area.setTiles(tiles);

    this.area = area;
*/
  };

  Game.prototype.setArea = function(area) {
    this.area = area;
  };

  Game.prototype.getArea = function() {
    return this.area;
  };

  Game.prototype.enterArea = function(number, player) {
    if (!this.area) this._createArea(number);
    this.area.insertEntity(player);
  };

  Game.prototype.leaveArea = function(player) {
    if (!this.area) return;
    this.area.removeEntity(player);  
  };

  Game.prototype.getPlayer = function(id) {
    return this.players[id];
  };

  Game.prototype.join = function(player) {
    if (this.players[player.id]) return;
    this.players[player.id] = player;
    if (this.area) {
      this.area.insertEntity(player);
    }
  };

  Game.prototype.leave = function(player) {
    delete this.players[player.id];
    if (this.area) {
      this.area.removeEntity(player);
    }
  };

  return Game; 

});
