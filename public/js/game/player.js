define(['./entity/creature'], function(Creature) {

  var __id = 0;

  var Player = function() {
    this.id = ++__id;
    this.avatar;
  };

  Player.prototype = {
    name: function() {
      return this.avatar.name;
    },

    use: function(entity) {
      this.avatar = entity;
      entity.is_player = true;
    }
  };

  return Player;

});
