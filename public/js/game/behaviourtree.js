define([], function() {


  var BehaviourTree = function(specs) {
    this.branches = [];
    
    for (var i in specs) {
      this.branches.push(new Branch(specs[i]));
    }
  };

  BehaviourTree.FAILED  = 0,
  BehaviourTree.SUCCESS = 1,
  BehaviourTree.RUNNING = 2;

  BehaviourTree.prototype = {
    reset: function() {
      for (var i in this.branches) {
        this.branches[i].reset();
      }
    },
    
    execute: function(object) {
      var result;
      
      for (var i in this.branches) {
        result = this.branches[i].execute(object);
        
        // FAILED - returned false. Continue with next branch.
        if (result == BehaviourTree.FAILED) continue;
        
        // SUCCESS - returned true.  Exit branch
        if (result == BehaviourTree.SUCCESS) return BehaviourTree.SUCCESS;
        
        // RUNNING - returned anything else.  Exit as running
        if (result == BehaviourTree.RUNNING) return BehaviourTree.RUNNING;
      }
    },
  };



  var Branch = function(commands) {
    this.leaves = [];
    this.current = 0;
    
    for (var i in commands) {
      var command = (commands[i] instanceof Array)
        ? new BehaviourTree(commands[i])
        : commands[i];
      
      this.leaves.push(command);
    }
  };

  Branch.prototype = {
    reset: function() {
      this.current = 0;
      for (var i in this.leaves) {
        if (this.leaves[i] instanceof Branch) this.leaves[i].reset();
      }
    },
    
    execute: function(object) {
      var result, leaf;
      
      for (var i=this.current, len=this.leaves.length; i<len; ++i) {
        result = this._exec(object, this.leaves[i]);
              
        // FAILED - returned false.  Exit branch.
        if (result == BehaviourTree.FAILED) {
          this.current = 0;
          return BehaviourTree.FAILED;
        }
        
        // SUCCESS - returned true.  Exit branch
        if (result == BehaviourTree.SUCCESS) continue;
        
        // RUNNING - returned anything else.  Exit as running
        if (result == BehaviourTree.RUNNING) {
          this.current = i;
          return BehaviourTree.RUNNING;
        }
      }
      
      this.current = 0;
      return BehaviourTree.SUCCESS;
    },
    
    _exec: function(object, func) {
      if (func instanceof Branch) return func.execute(object);
      if (!object[func]) throw new Error('Object does not implement method: '+ func);
      return object[func].call(object);
    }
  };

  return BehaviourTree;

});
