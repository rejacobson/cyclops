define(['underscore'], function(_) {

  //                  [w, h]   [w, h]
  var Grid = function(mapsize, cellsize, offset) {
    // The number of cells in each of the x and y axis
    var cell_count_x = Math.round(mapsize[0] / cellsize[0]),
        cell_count_y = Math.round(mapsize[1] / cellsize[1]);

    // The size of the area being partitioned.
    this.mapsize = mapsize;

    // Map offset for calculating entity cell placement using their positions
    this.offset = offset || [0, 0];

    // The number of cells in each of the x and y axis
    this.cellcount = [cell_count_x, cell_count_y];

    // Recalculate the cell sizes based off of the x and y cell counts
    this.cellsize = [mapsize[0] / cell_count_x, mapsize[1] / cell_count_y];

    // The largest cell index in the map
    this.max = this.cellcount[0] * this.cellcount[1] - 1;

    // Keep track of the number of elements in each cell
    // residents[cell_index] == number of elements
    this.residents = {};
    
    // Array of grid cells
    // This holds the entities/values being partitioned
    this.map = [];
  };

  Grid.prototype._info = function() {
    var info = {
      mapsize: this.cellcount.slice(0),
      cellsize: this.cellsize.slice(0),
      cells: []
    };

    for (var i=0; i<this.max; ++i) {
      info.cells.push(this._positionByIndex(i));
    }

    return info; 
  };

  // Add a new element to the grid, in the specified cell index
  Grid.prototype._insert = function(index, value) {
    var cell = this._cell(index, []);
    cell.push(value);
    if (!this.residents[index]) this.residents[index] = 0;
    this.residents[index]++; // increment the number of elements in this cell
  };

  // Remove an element from the grid and return it
  Grid.prototype._remove = function(index, value) {
    var cell = this._cell(index);

    for (var i=0; i<cell.length; ++i) {
      if (cell[i] == value) {
        cell.splice(i, 1);
        this.residents[index]--;
        if (this.residents[index] <= 0) delete this.residents[index];
        break;
      }
    }

    return value;
  };

  // Return the stored element counts.
  // Only cells with more than one element in them are represented.
  Grid.prototype._population = function() {
    return this.residents;
  };

  // Get the contents of a cell, or cells, by it's index
  Grid.prototype._cell = function(index) {
    // Return multiple indices
    if (_.isArray(index)) {
      var list = [], cell;
      for (var i=0, len=index.length; i<len; ++i) {
        cell = index[i];
        if (this.map[cell] && this.map[cell].length) {
          list = list.concat(this.map[cell]);
        }
      };
      return list;
    }
    
    if (this.map[index] == undefined) this.map[index] = [];

    // Return a single index
    return this.map[index];
  };

  // Get the contents of all cells surrounding the cell at index
  Grid.prototype._surroundingCells = function(index) {
    return this._cell(this._surroundingIndices(index));
  };

  // Get a list of all indices surrounding the cell at index 
  Grid.prototype._surroundingIndices = function(index) {
    var indices = [],
        coords = this._coordinatesByIndex(index),
        x = coords[0],
        y = coords[1],
        width = this.cellcount[0],
        height = this.cellcount[1];

    // X is not touching left hand side
    if (x > 1) {
      indices.push(index - 1); // WEST
      
      if (y > 1) indices.push(index - width - 1); // NORTH WEST
      if (y < height - 1) indices.push(index + width - 1); // SOUTH WEST
    }

    // X is not touching the right hand side
    if (x < width - 1) {
      indices.push(index + 1); // EAST
     
      if (y > 1) indices.push(index - width + 1); // NORTH EAST
      if (y < height - 1) indices.push(index + width + 1); // SOUTH EAST
    }

    // Y is not touching the top
    if (y > 1) indices.push(index - width); // NORTH

    // Y is not touching the bottom
    if (y < height - 1) indices.push(index + width); // SOUTH
    
    return indices;
  };

  // Get a cell x and y coordinates by a cell index
  Grid.prototype._coordinatesByIndex = function(index) {
    var y = Math.floor(index / this.cellcount[0]),
        x = index - (y * this.cellcount[0]);
    return [x, y];
  };

/*
  Grid.prototype._lookup = function() {
    // Use an index to find x:y coordinates
    if (arguments.length === 1) {
      return [arguments[0] % this.cellcount[0], arguments[0] / this.cellcount[0] | 0];

    // Use x:y coordinates to find an index
    } else {
      return arguments[1] * this.cellcount[0] + arguments[0];    
    }
  };
*/

  // Get the cell index by a position relative to the map size
  var offset_pos = [0, 0];
  Grid.prototype._indexByPosition = function(position) {
    offset_pos[0] = position[0] + this.offset[0];
    offset_pos[1] = position[1] + this.offset[1];
 
    if (offset_pos[0] < 0 || offset_pos[0] > this.mapsize[0] || offset_pos[1] < 0 || offset_pos[1] > this.mapsize[1]) return null;

    var x = Math.floor(offset_pos[0] / this.cellsize[0]),
        y = Math.floor(offset_pos[1] / this.cellsize[1]);
        
    var index = (y * this.cellcount[0]) + x;

    // Return null if the position is out of bounds
    if (index < 0 || index > this.max) return null;

    return index;
  };

  // Get a cell position relative to the map size by a cell index
  // Returns the position of the top left of the cell
  Grid.prototype._positionByIndex = function(index) {
    var coords = this._coordinatesByIndex(index);
    return [coords[0] * this.cellsize[0] - this.offset[0], coords[1] * this.cellsize[1] - this.offset[1]];
  };

  return Grid;

});
