define(['underscore'], function(_) {
  /**
    var list = new GroupedList();

    list.push(cat);
    list.push(sword);
    list.push(dog);
    list.push(tree);
    list.push(fence);
    list.remove(rat);

    list.get('creatures'); // [cat, dog]
    list.get('items);  // [sword]
    list.get('structures'); // [tree, fence]
   */
  var _ids = 0;

  var GroupedList = function(items) {
    this.id = ++_ids;

    this.list = [];
    this.groups = {};

    if (items && items instanceof Array && items.length) {
      for (var i in items) {
        this.push(items[i]);
      }
    }
  };

  GroupedList.prototype = {
    push: function(obj) {
      if (!obj.groups) throw new Error('GroupedList#push -- obj has no `groups` property or method');
      if (obj[this._index()]) throw new Error('GroupedList#push -- obj is already in this GroupedList');
      this._add_to_list(obj);
      this._add_to_group(obj);
    },
    
    remove: function(obj) {
      this._remove_from_list(obj);
      this._remove_from_group(obj);
      return obj;
    },
    
    /**
     * list.get()                        // all items
     * list.get('group_name')            // all items in a group
     * list.get({key: value})            // all items having the keys and values of the passed in object
     * list.get('group_name', {key: value});  // items in a group having the keys and values
     */
    get: function(a, b) {
      if (arguments.length == 0) return this.list;
      if (a instanceof String) return this.groups[a];
      if (arguments.length == 1 && _.isPlainObject(a)) return _.where(this.list, a);
      if (arguments.length == 2 && a instanceof String && _.isPlainObject(b)) return _.where(this.groups[a], b);
    },

    _index: function() {
      return '__grouped_list_index_'+ this.id;
    },

    _add_to_list: function(obj) {
      obj[this._index()] = this.list.length;
      this.list.push(obj);
    },

    _add_to_group: function(obj) {
      var groups = _.isFunction(obj.groups) ? obj.groups() : obj.groups,
          group;

      for (var i in groups) {
        group = groups[i];

        if (!this.groups[group]) this.groups[group] = [];
        this.groups[group].push(obj);
      }
    },
    
    _remove_from_list: function(obj) {
      var index = this._index();
      if (obj[index] === undefined) return;
      this.list.splice(obj[index], 1);
      obj[index] = undefined;
    },
    
    _remove_from_group: function(obj) {
      var groups = _.isFunction(obj.groups) ? obj.groups() : obj.groups,
          group;

      for (var i in groups) {
        group = groups[i];

        for (var j in this.groups[group]) {
          if (this.groups[group][j] == obj) {
            this.groups[group].splice(j, 1);
            break;
          }
        }
      }

    }    
  };
      
  return GroupedList;

});
