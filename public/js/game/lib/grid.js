define(['underscore', '../mixins/stateful'], function(_, Stateful) {

  var Grid = function(width, height, defaultValue) {
    this.width = width;
    this.height = height;

    this.length = width * height;
    this.size = [width, height];

    this._map = [];
  };

  Grid.prototype = {

    _toIndex: function(x, y) {
      return y * this.width + x;
    },

    insert: function(x, y, item) {
      var index = this._toIndex(x, y);
      if (item.__index == index) return;

      this.remove(item);

      var cell = this.get(index);
      cell.push(item);
      item.__index = index;
    },

    remove: function(item) {
      if (!item.__index || !this._map[item.__index] || this._map[item.__index].length == 0) return;

      var cell = this.get(item.__index);

      for (var i in cell) {
        if (cell[i] == item) {
          cell.splice(i, 1);
          item.__index = undefined;
          break;
        }
      }
    },

    forEachCell: function(callback) {
      for (var i=0; i<this.length; ++i) {
        callback(this.get(i), this.metadata(i), i);
      }
    },

    forEachItem: function(x, y, callback) {
      var cell = this.get(x, y);
      var metadata = this.metadata(x, y);

      for (var i in cell) {
        callback(cell[i], metadata, i);
      }
    },

    get: function(x, y) {
      var index = (arguments.length == 1) ? x : this._toIndex(x, y);
      if (!this._map[index]) this._map[index] = [];
      return this._map[index];
    }

  };

  return Grid;

});

