define(['../lib/inheritance', './base'], function(Class, Entity) {

  var Item = Class.extend(Entity, {
    initialize: function(args) {
      this.parent(args);
      this.joinGroup('items');
      this.blocking = false;
    }
  });

  return Item;

});


