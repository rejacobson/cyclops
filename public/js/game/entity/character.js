define(['../event', '../lib/inheritance', './creature'], function(Event, Class, Creature) {

  var Character = Class.extend(Creature, {
    initialize: function(args) {
      this.parent(args);
      this.joinGroup('characters');
    },

    performed: function() {
      Event.trigger('character.performed', arguments);
    },

    isIdle: function() {
      return false;
    } 
  });

  return Character;

});
