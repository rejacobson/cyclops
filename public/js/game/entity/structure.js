define(['../lib/inheritance', './base', '../data/structures'], function(Class, Entity, Structures) {

  var Structure = Class.extend(Entity, {
    initialize: function(args) {
      this.parent(args);
      this.joinGroup('structures');
    }
  });

  Structure.create = function(name) {
    if (!Structures[name]) return null;
    return new Structure(_.merge({
      name: name
    }, Structures[name])); 
  };

  return Structure;

});


