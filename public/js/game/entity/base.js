define(['underscore', '../lib/inheritance', '../lib/vector2d', '../mixins/stats', '../mixins/groupable'], function(_, Class, Vector2d, Stats, Groupable) {

  var __id = 0;

  var _stats = {
    hp: 1
  };

  var Entity = Class.extend({
    include: [Stats, Groupable],

    initialize: function(args) {
      this.id       = (++__id);
      this.name     = args.name || 'entity';
      this.type     = args.type || this.name;
      this.position = Vector2d(args.position || [-1, -1]);
      this.area     = args.area;

      this.blocking     = args.blocking || true;
      this.flammability = args.flammability || 0;

      // Stats
      this.baseStats(_.merge({}, _stats, args.stats || {}));

      // Add Groups
      if (args.group) {
        if (!(args.group instanceof Array)) args.group = [args.group];
        var self = this;
        args.group.forEach(function(name) {
          self.joinGroup(name);
        });
      }

      for (var i in args) {
        if (_.isFunction(args[i])) this[i] = args[i];
      }
    },

    setPosition: function(x, y) {
      if (x instanceof Array) {
        this.position.set(x);
      } else {
        this.position.set([x, y]);
      }
    }
  });

  return Entity;

});
