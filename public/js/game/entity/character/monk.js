define(['../../lib/inheritance', '../character'], function(Class, Character) {

  var Monk = Class.extend(Character, {
    initialize: function(args) {
      this.parent(args);
      this.type = 'monk';
    }
  });

  return Monk;

});

