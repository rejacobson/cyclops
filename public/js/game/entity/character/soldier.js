define(['underscore', '../../lib/inheritance', '../character'], function(_, Class, Character) {

  var Soldier = Class.extend(Character, {
    initialize: function(args) {
      this.parent(args);
      this.type = 'soldier';
    }
  });

  return Soldier;

});

