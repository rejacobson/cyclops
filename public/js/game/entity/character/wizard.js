define(['../../lib/inheritance', '../character'], function(Class, Character) {

  var Wizard = Class.extend(Character, {
    initialize: function(args) {
      this.parent(args);
      this.type = 'wizard';
    }
  });

  return Wizard;

});

