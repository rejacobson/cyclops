define(['../../lib/inheritance', '../character'], function(Class, Character) {

  var Ranger = Class.extend(Character, {
    initialize: function(args) {
      this.parent(args);
      this.type = 'ranger';
    }
  });

  return Ranger;

});

