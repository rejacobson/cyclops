define(['../../lib/inheritance', '../character'], function(Class, Character) {

  var Thief = Class.extend(Character, {
    initialize: function(args) {
      this.parent(args);
      this.type = 'thief';
    }
  });

  return Thief;

});

