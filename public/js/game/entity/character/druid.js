define(['../../lib/inheritance', '../character'], function(Class, Character) {

  var Druid = Class.extend(Character, {
    initialize: function(args) {
      this.parent(args);
      this.type = 'druid';
    }
  });

  return Druid;

});
