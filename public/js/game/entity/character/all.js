define(['./druid', './monk', './ranger', './soldier', './thief', './wizard'],
function(Druid, Monk, Ranger, Soldier, Thief, Wizard) {

  return {
    Druid: Druid,
    Monk: Monk,
    Ranger: Ranger,
    Soldier: Soldier,
    Thief: Thief,
    Wizard: Wizard
  };

});
