define(['underscore', '../lib/inheritance', './base', '../data/creatures', '../mixins/timeable', '../event', '../actions'],
function(_, Class, Entity, Creatures, Timeable, Event, Actions) {

/*
  var _stats = {
    attack: 1,
    defense: 1,

    fatique: 0,

    mass: 0,

    strength: 0,
    intelligence: 0,
    dexterity: 0,

    power: function() {
      return this.stat('strength') + this.stat('intelligence');
    },

    stealth: function() {
      return this.stat('intelligence') + this.stat('dexterity');
    },

    speed: function() {
      return this.stat('strength') + this.stat('dexterity'); 
    },   

    capacity: function() {
      return this.stat('strength') * 10;  
    }
  };
*/

  var _stats = {
    attack: 1,
    defense: 1,

    str: 1,     // 1 - 10 
    int: 1,     // 1 - 10
    agi: 1,     // 1 - 10

    // Used to calculate how many actions per turn a creature can take
    speed: 1
  };

  var Creature = Class.extend(Entity, {
    include: [Timeable],

    initialize: function(args) {
      this.parent(args);
      this.joinGroup('creatures');
      this.baseStats(_stats);
      this.initTimer();
    },

    act: function() {},
    
    // speed 15  == time 4.5
    // speed 10  == time 2 
    // speed 5   == time 1.3
    // speed 1   == time 1
    // speed 0   == time 0.9
    // speed -1  == time 0.9
    // speed -5  == time 0.7
    // speed -10 == time 0.6
    // speed -15 == time 0.5
    tick: function() {
      var speed = Math.max(Math.min(this.stat('speed'), 15), -15),
          time = 1 / (1 - 0.0555556 * (speed-1)),
          time = Math.round(time * 10) / 10;

      this.addTime(time);
    },
    
    perform: function(action, params) {
      if (!this.isReady()) return;

      var args = Array.prototype.slice.call(arguments, 1);

      this._perform.apply(this, [action, Math.random()*1000000].concat(args));
    },

    _perform: function(action, seed, params) {
      var args = Array.prototype.slice.call(arguments, 1);
      
      var result = Actions[action].apply(this, args);
      
      if (result.time) {
        this.useTime(result.time);

        args = [this.id, action].concat(args);

        Event.trigger('entity.performed', args);

        if (this.performed) this.performed.apply(this, args);
      }
    }

  });

  Creature.create = function(name) {
    if (!Creatures[name]) return null;
    return new Creature(_.merge({
      name: name
    }, Creatures[name])); 
  };

  return Creature;

});

