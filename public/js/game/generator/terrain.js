define(['underscore', '../data/terrain'], function(_, Terrain) {

  var TerrainGenerator = function(options) {
    var _defaults = {
          height: 20,
          width: 20,
          terrain: 'grass'
        };

    this.options = _.merge(_defaults, options);
    this.tiles = [];

    for (var y=0; y<this.options.height; ++y) {
      for (var x=0; x<this.options.width; ++x) {
        this.tiles.push(this.options.terrain);
      }
    }
  };

  TerrainGenerator.prototype = {
    width:  function() {
      return this.options.width;
    },

    height: function() {
      return this.options.height;
    },

    spawn:  function() {
      return [this.options.width/2, this.options.height/2];
    },

    tile:   function(i) {
      return this.tiles[i];
    },
  
    properties: function(type) {
      return _.merge({}, Terrain._default, Terrain[type]);
    }

  };

  return TerrainGenerator;

});
