define(['underscore', '../area'], function(_, Area) {

  var AreaGenerator = function(options) {
    options = _.merge({
      width: 50,
      height: 50
    }, options);

    var area = new Area(options.width, options.height);

    generateTerrain(area, options);
    generateStructures(area, options);
    generateCreatures(area, options); 

    return area;
  };

  function generateTerrain(area, options) {
    var tile;
    for (var x=0, xlen=options.width; x<xlen; ++x) {
      for (var y=0, ylen=options.height; y<ylen; ++y) {
        tile = area.getTile(x, y);
        tile.terrain('grass');
      }   
    }   
  };

  function generateStructures(area, options) {

  };

  function generateCreatures(area, options) {

  };

  return AreaGenerator;

});
