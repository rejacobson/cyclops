define(['./lib/rng'], function(RNG) {

  return {
    'move': function($seed, direction) {
      var x = this.position.x,
          y = this.position.y;

      switch (direction) {
        case 1: x-=1; y+=1; break;
        case 2: y+=1;       break;
        case 3: x+=1; y+=1; break;
        case 4: x-=1;       break;
        case 6: x+=1;       break;
        case 7: x-=1; y-=1; break;
        case 8: y-=1;       break;
        case 9: x+=1; y-=1; break;
      }

/*
      if (this.area.hasCreature(x, y)) {
        return this.perform('attack', [x, y]);
      }

      if (this.area.isBlocking(x, y)) return 0;
*/

      this.area.place(this, x, y); 

      return { time: 1 };
    }
  };

});
