define(['./event'], function(Event) {

  var Director = function(area) {
    this.area = area;
    this.players = {};
    this.actionlog;
    
    var self = this;
    Event.on('character.performed', function() {
      if (self._readyPlayers().length == 0) {
        var result = self.simulate();
        Event.trigger('simulated', [result]);
      }
    });
  };

  Director.prototype = {
    _readyPlayers: function() {
      var players = [], p;
      
      for (var i in this.players) {
        p = this.players[i];
        if (!p.isIdle() && p.isReady()) players.push(p);
      }
      
      return players;
    },
    
    addPlayer: function(player) {
      this.players[player.id] = player;
      this.area.place(player);
    },
    
    removePlayer: function(player) {
      delete this.players[player.id];
      this.area.remove(player);
    },  
    
    simulate: function() {
      var actionlog = [],
          result;
      
      this.area.entities.forEach(function(e) { 
        if (e.act) {

          // Take a turn
          while (e.isReady() && (result = e.act())) {
            actionlog.push(result);
          }

          if (e.isReady()) e.setTime(0);
        
          // Increment timer
          e.tick();
        }      
      }, this);
      
      return actionlog;
    }
  };

  return Director;

});
