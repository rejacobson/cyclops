define(['underscore'], function(_) {

  var TileCollection = function(tiles) {
    this.tiles = tiles;
    this.length = tiles.length;
  };

  TileCollection.prototype = {
    filter: function(filter_func) {
      return new TileCollection(_.filter(this.tiles, filter_func));
    },

    getTilesByGroup: function(group) {
      var list = [];
      this.tiles.forEach(function(tile) {
        list.concat(tile.get(group));
      });
      return list;
    }
  };

  return TileCollection;

});
