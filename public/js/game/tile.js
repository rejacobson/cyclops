define(['underscore', './lib/groupedlist', './data/terrain', './entity/item'], function(_, GroupedList, Terrain, Item) {

  var Tile = function() {
    this.entities = [];
    this._terrain = '';
  };
  
  Tile.prototype = {
    insert: function(entity) {
      if (entity instanceof Item) {
        this.entities.unshift(entity);
      } else {
        this.entities.push(entity);
      }
    },

    remove: function(entity) {
      for (var i in this.entities) {
        if (this.entities[i] == entity) {
          this.entities.splice(i, 1);
          break;
        }
      }
    },

    terrain: function(name) {
      if (name && Terrain[name]) this._terrain = _.merge({name: name}, Terrain['default'], Terrain[name]);
      return this._terrain;
    }
  };

  return Tile;
});

