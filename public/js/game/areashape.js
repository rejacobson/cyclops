define(function() {

  // http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
  var line = function(start, end) {
    var dx = Math.abs(end[0] - start[0]),
        dy = Math.abs(end[1] - start[1]),
        sx = (start[0] < end[0]) ? 1 : -1,
        sy = (start[1] < end[1]) ? 1 : -1,
        err = dx - dy,
        x = start[0],
        y = start[1],
        points = [],
        e2;
     
    while (true) {
      points.push([x, y]);
      
      if (x == end[0] && y == end[1]) break;
      
      e2 = 2 * err;
      
      if (e2 > -dy) {
        err -= dy;
        x += sx;
      }
      
      if (e2 < dx) {
        err += dx;
        y += sy;
      }
    };
    
    return points;
  };

  var rectangle = function(p1, p2) {
    var points = [],
        sx = Math.min(p1[0], p2[0]),
        sy = Math.min(p1[1], p2[1]),
        ex = Math.max(p1[0], p2[0]),
        ey = Math.max(p1[0], p2[0]);

    for (var x=sx; x<=ex; ++x) {
      for (var y=sy; y<=ey; ++y) {
        points.push([x, y]);  
      }
    }

    return points;
  };

  var circle = function(center, radius) {
    var cx = center[0],
        cy = center[1],
        r2 = Math.pow(radius + 0.45, 2),
        max = Math.sqrt( radius*radius / 2 ) + 1,
        points = new PointCollection();

    points.push([cx, cy + radius]);
    points.push([cx, cy - radius]);
    points.push([cx + radius, cy]);
    points.push([cx - radius, cy]);
     
    for (var dy=1; dy<max; ++dy) {
      for (var dx=radius; dx>0; --dx) {
        if ((dx*dx + dy*dy) > r2) continue;

        points.push([cx + dx, cy + dy]);
        points.push([cx - dx, cy + dy]);
        points.push([cx + dx, cy - dy]);
        points.push([cx - dx, cy - dy]);

        if (dx == dy) break;

        points.push([cx + dy, cy + dx]);
        points.push([cx - dy, cy + dx]);
        points.push([cx + dy, cy - dx]);
        points.push([cx - dy, cy - dx]);

        break;
      }
    }

    return points.points();
  };

  var disc = function(center, radius) {
    var cx = center[0],
        cy = center[1],
        x = cx,
        y = cy,
        r2 = Math.pow(radius + 0.45, 2),
        points = [[cx, cy]];

    for (var i=1; i<=radius; ++i) {
      points.push([cx + i, cy]);
      points.push([cx - i, cy]);
      points.push([cx, cy + i]);
      points.push([cx, cy - i]);
    }

    for (var dx=1; dx<=radius; ++dx) {
      for (var dy=1; dy<=radius; ++dy) {
        if ((dx*dx + dy*dy) > r2) continue;
        points.push([cx + dx, cy + dy]);
        points.push([cx - dx, cy + dy]);
        points.push([cx + dx, cy - dy]);
        points.push([cx - dx, cy - dy]);
      }
    }

    return points;
  };

  // Save a collection of points without duplicates.
  var PointCollection = function() {
    var points = [];
    var hash = {};

    this.length = 0;

    function to_hash(p) {
      return p[0] +':'+ p[1];
    };

    this.push = function(point) {
      var h = to_hash(point);
      if (hash[h]) return;
      hash[h] = 1;
      points.push(point);
      this.length++;
    };
    
    this.points = function() {
      return points;
    };
  };

  return {
    line: line,
    rectangle: rectangle,
    circle: circle,
    disc: disc
  };

});
