define(['underscore'], function(_) {

  var EntityManager = function() {
    this._entities = [];
    this._entityid = {};
    this._entitygroup = {};
  };

  EntityManager.prototype = {
    insert: function(entity) {
      if (this._entityid[entity.id]) return;

      this._entities.push(entity);
      this._entityid[entity.id] = entity;

      var self = this;
      entity.forEachGroup(function(group) {
        if (!self._entitygroup[group]) self._entitygroup[group] = [];
        self._entitygroup[group].push(entity);
      });
    },

    remove: function(entity) {
      var id = _.isNumber(entity) ? entity : entity.id;

      if (!this._entityid[id]) return;

      var entity = this._entityid[id];

      // Remove from ids
      delete this._entityid[id];

      // Remove from entities
      for (var i in this._entities) {
        if (this._entities[i] == entity) {
          this._entities.splice(i, 1);
          break;
        }
      }

      // Remove from groups
      var self = this, list;
      entity.groups().forEach(function(group) {
        list = self._entitygroup[group];

        for (var i in list) {
          if (list[i] == entity) {
            list.splice(i, 1);
            break;
          }
        }
      });
    },

    getEntity: function(entity) {
      var id = _.isNumber(entity) ? entity : entity.id;
      return this._entityid[id];
    },

    getGroup: function(group) {
      return this._entitygroup[group];
    },
    
    forEach: function(callback, context) {
      context = context || this;
      for (var i in this._entities) {
        callback.call(context, this._entities[i], i);
      }
    }
  };

  return EntityManager; 

});
