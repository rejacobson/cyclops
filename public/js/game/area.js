define(['./event', './tilemap', './entitymanager'], function(Event, TileMap, EntityManager) {
  
  var Area = function(width, height) {
    this.entities = new EntityManager();
    this.map = new TileMap(width, height);
  };

  Area.prototype = {
    place: function(entity, x, y) {
      if (!this.entities.getEntity(entity)) {
        this.entities.insert(entity);
        entity.area = this;
      }

      x = x || entity.position.x;
      y = y || entity.position.y;

      this.map.insert(entity, x, y); 
      entity.setPosition(x, y);

      Event.trigger('entity.placed', [entity]);
    },

    remove: function(entity) {
      this.entities.remove(entity);
      this.map.remove(entity);
      entity.area = undefined;
    },

    getEntity: function(entity) {
      return this.entities.get(entity);
    },

    tileExists: function(x, y) {
      return this.map.exists(x, y);
    },

    getTile: function(x, y) {
      return this.map.getTile(x, y);
    },

    getTiles: function(coords) {
      var tiles = [], tile;
      for (var i in coords) {
        tile = this.getTile(coords[i][0], coords[i][1]);
        if (!tile) continue;
        tiles.push(this.getTile(coords[i][0], coords[i][1])); 
      }
      return tiles;
    },

    tick: function() {
      var entity = this.entities[this.current_entity]; 
      this.current_entity++;
    },

    gameLoop: function() {
      
    }
  };

  return Area;

});
