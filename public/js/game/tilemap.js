define(['./tile'], function(Tile) {

  var TileMap = function(width, height) {
    this.width = width;
    this.height = height;
    this.grid = [];
    
    for (var i=0, len=width*height; i<len; ++i) {
      this.grid.push(new Tile());
    }
  };

  TileMap.prototype = {
    _index: function(x, y) {
      if (!this.exists(x, y)) throw new Error('TileMap -- Trying to get an index on, ['+ x +', '+ y +'], which is out of the map bounds, ['+ this.width +', '+ this.height +']');
      return y * this.width + x;
    },

    exists: function(x, y) {
      return !(x >= this.width || x < 0 || y >= this.height || y < 0);
    },
    
    getTile: function(x, y) {
      var index = arguments.length == 1 ? x : this._index(x, y);
      return this.grid[index];
    },
    
    insert: function(entity, x, y) {
      var index = this._index(x, y);
      if (entity.__tile_map_index) this.remove(entity);
      this.getTile(index).insert(entity);
      entity.__tile_map_index = index;
    },
    
    remove: function(entity) {
      if (!entity.__tile_map_index) return;
      this.getTile(entity.__tile_map_index).remove(entity);
      entity.__tile_map_index = undefined;
    }
  };

  return TileMap;

});
