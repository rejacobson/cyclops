define(function() {

  return {
    'default': {
      blocking: false, // Creatures cannot move into, or through, blocking tiles
      los: true,       // Line of Sight (LOS): Can it been seen through?
      speed: 1,        // Movement modifier:  1 == unrestricted movement; 0 == cannot move at all; 0.5 == slowed down by half normal movement speed; >1 == Aids in movement
      noise: 0.5
    },

    'grass': {
      noise: 0.1
    },

    'tall grass': {
      los: false,
      speed: 1,
      noise: 0.15
    },

    'burnt grass': {
      noise: 0.6
    },

    'mud': {
      speed: 0.75,
      noise: 0.4
    },

    'stone': {
      noise: 0.5
    },

    'sand': {
      speed: 0.9,
      noise: 0.3
    }
  };

});
