define({
  'rat': {
    level: 1,
    stats: {
      hp: 5,
      str: 2,
      dex: 9
    },
    act: function() {
      console.log('Rat is acting!');
      return false;
    }
  }
});
