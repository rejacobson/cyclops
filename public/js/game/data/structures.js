define({
  /**
   * Trees
   */
  'oak': {
    groups: ['trees'],
    flammability: 0.6,
    stats: {
      hp: 2000
    }
  },

  'pine': {
    groups: ['trees'],
    flammability: 0.7,
    stats: {
      hp: 1500
    }
  },


  /**
   * Walls
   */
  'granite': {
    groups: ['walls'],
    stats: {
      hp: 10000
    }
  },

  'shale': {
    groups: ['walls'],
    stats: {
      hp: 500
    }
  },

  'wood wall': {
    groups: ['walls'],
    flammability: 0.7,
    stats: {
      hp: 700
    }
  }
});
