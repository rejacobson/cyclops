var GOAP = {};

/******************************************************************************
 * Action
 ******************************************************************************/
GOAP.Action = function(name, spec) {
  this.name       = name;
  this.conditions = new GOAP.State(spec.conditions);
  this.effects    = new GOAP.State(spec.effects);

  this.edges = [];
};

GOAP.Action.prototype = {
  addEdges: function(nodes) {
    if (!(nodes instanceof Array)) nodes = [nodes]; 
    this.edges = this.edges.concat(nodes);
  },

  getEdges: function() {
    return this.edges;
  }
};

GOAP.Action.graph = function(action_set) {
  var graph = {}, key_effects = {};

  // Build the initial graph; no edges
  for (var name in action_set) {
    graph[name] = new GOAP.Action(name, action_set[name]);
  }

  // Build the key-effects table
  for (var name in graph) {
    graph[name].effects.forEachKey(function(key, value) {
      if (key_effects[key] == undefined) key_effects[key] = [];
      key_effects[key].push(name);
    });
  }

  // Connect the graph nodes with edges
  var node, edge;
  for (var name in graph) {
    node = graph[name];

    node.conditions.forEachKey(function(key, value) {
      for (var i in key_effects[key]) {
        edge = graph[key_effects[key][i]];
        if (node.conditions.containsSome(edge.effects)) node.addEdges(key_effects[key][i]);
      }
    });
  }

  return graph;
};


/******************************************************************************
 * Planner
 ******************************************************************************/
GOAP.Planner = function(action_set) {
  this.graph = GOAP.Action.graph(action_set);
};

GOAP.Planner.prototype = {
  buildPlan: function(goal, current) {
    var plannergraph = new AStarPlannerGraph(this.graph);
  }
};


/******************************************************************************
 * State
 ******************************************************************************/
GOAP.State = function(keys) {
  this.keys = keys;
};

GOAP.State.prototype = {
  forEachKey: function(callback, context) {
    context = context || this;
    for (var key in this.keys) {
      if (this.keys[key] !== undefined) callback.call(context, key, this.keys[key]);
    }
  },

  containsAll: function(state) {
    for (var key in state.keys) {
      if (this.keys[key] !== state.keys[key]) return false;
    }
    return true;
  },

  containsSome: function(state) {
    for (var key in state.keys) {
      if (this.keys[key] === state.keys[key]) return true;
    }
    return false;
  },

  countMatches: function(state) {
    var matches = 0;
    state.forEachKey(function(key, value) {
      if (value === this.keys[key]) matches++;
    }, this);
    return matches;
  },

  countMisses: function(state) {
    var misses = 0;
    state.forEachKey(function(key, value) {
      if (value !== this.keys[key]) misses++;
    }, this);
    return misses;
  },

  add: function(state) {
    for (var key in state.keys) {
      if (state.keys[key] !== undefined) this.keys[key] = state.keys[key];
    }
    return this;
  }
};

GOAP.State.keys = [];



var AStarPlannerGraph = function(graph) {
  this.graph      = graph;
  this.astarnodes = {};
};

AStarPlannerGraph.prototype = {
  getNode: function(id) {
    if (!this.astarnodes[id]) {
      var node = this.graph[id],
          astarnode = new AStarPlannerNode({
            id: id,
            g: 0,
            goal_state: node.conditions,
            current_state: node.effects
          });
      
      this.astarnodes[id] = astarnode;
    }
    
    return this.astarnodes[id];
  },
  
  getNeighborNodes: function(id) {
    var edges = this.graph[id].getEdges(), neighbors = [];
    for (var i in edges) neighbors.push(this.getNode(edges[i]));
    return neighbors;
  }
};



var AStarPlannerNode = function(args) {
  args = args || {};
  this.id            = args.id || null;
  this.parent        = args.parent || null;
  this.f             = args.f || 0;
  this.g             = args.g || 0;
  this.h             = args.h || 0;
  this.current_state = args.current_state || null;
  this.goal_state    = args.goal_state || null;
};

AStarPlannerNode.prototype = {
  distanceTo: function(node) {
    return this.goal_state.countMisses(node.current_state);
  },
  
  heuristicCost: function(node) {
    return this.distanceTo(node);
  }
};



var AStarStorage = function() {
  this.open_set = [];
  this.closed_set = [];
  
  this.openSetLength = function() {
    return this.open_set.length;
  }
  
  this.addToOpenSet = function(node) {
    this.open_set.push(node);
  }
  
  this.isInOpenSet = function(node) {
    for (var i in this.open_set) {
      if (node == this.open_set[i]) return true;
    }
  }
  
  this.removeFromOpenSet = function(node) {
    for (var i in this.open_set) {
      if (node == this.open_set[i]) {
        this.open_set.splice(i, 1);
        break;
      }
    }
  }
  
  this.getCheapestOpenNode = function() {
    var f, node;
    for (var i in this.open_set) {
      if (f === undefined || this.open_set[i].f < f) node = this.open_set[i];
    }
    return node;
  }
  
  this.closedSetLength = function() {
    return this.closed_set.length;
  }  
  
  this.addToClosedSet = function(node) {
    this.closed_set.push(node);
  }
  
  this.isInClosedSet = function(node) {
    for (var i in this.closed_set) {
      if (node == this.closed_set[i]) return true;
    }
  }
  
  this.removeFromClosedSet = function(node) {
    for (var i in this.closed_set) {
      if (node == this.closed_set[i]) {
        this.closed_set.splice(i, 1);
        break;
      }
    }
  }
};


/*
var AStarGraph = function(map) {
  this.getNode = function(id) {
    var node = new AStarNode();
    node.params = ....;
    return node;
  }
};
var AStarNode = function() {
  this.distanceTo = function(node) {};
};

var AStarPlannerGraph = function(map) {};
var AStarPlannerNode = function() {};

var AStarNavGraph = function() {}
var AStarNavNode = function() {}


var astar = new AStarSolver();
astar.findPath(current, goal);


var goal = new AStarPlannerNode();
goal.id = 'goal';
goal.goal = goal.state;
goal.current = current.state;

var current = new AStarPlannerNode();
goal.id = 'current';
current.goal = current.state;
current.current = current.state;

var astar = new AStarSolver(plannergraph, storage);
astar.findPath(goal, current);       // params are swapped, so the current node becomes the AStar goal
*/

getNode: function(id) {
  if (id instanceof State) {
    
  } else {
    if (!this.astarnodes[id]) {
      var node = this.graph[id],
          astarnode = new AStarPlannerNode({
            id: id,
            g: 0,
            goal_state: node.conditions,
            current_state: node.effects
          });
      
      this.astarnodes[id] = astarnode;
    }
    
    return this.astarnodes[id];
  }
}


//                         +-- map structure to be searched
//                         |
//                         |      +-- current node on the map
//                         |      |        
//                         |      |        +-- goal location on the map
//                         |      |        | 
AStar.findPath = function(graph, current_graph_node, goal_graph_node) {
  var current_node = graph.getNode(current_graph_node);
      
  storage.addToOpenSet(current_node);
  
  var h = current_node.heuristicCost(goal);
  current_node.g = 0;
  current_node.f = h;
  current_node.h = h;
  
  while (storage.openSetLength()) {
    current_node = storage.getCheapestOpenNode();
    
    if (current_node.distanceTo(goal) == 0) return reconstruct_path(current_node);
    
    this.storage.removeFromOpenSet(current_node);
    this.storage.addToClosedSet(current_node);
    
    var neighbors = this.graph.getNeighborNodes(current_node),
        neighbor,
        tentative_g_score;
        
    for (var i in neighbors) {
      neighbor = neighbors[i];
      
      tentative_g_score = current_node.g_score + current_node.distanceTo(neighbor);
      
      if (this.storage.inClosedSet(neighbor)) {
        if (tentative_g_score >= neighbor.g) continue;
      }
      
      if (!this.storage.inOpenSet(neighbor) || tentative_g_score < neighbor.g) {
        neighbor.parent = current_node.id;
        neighbor.g = tentative_g_score;
        neighbor.f = neighbor.g + neighbor.heuristicCost(goal);
        if (!this.storage.inOpenSet(neighbor)) {
          this.storage.addToOpenSet(neighbor);
        }
      }
    }
  }
  
  return false;
};




