var Actions = {
  'ButcherCorpse': {
    conditions: {
      'isTargetDead': true,
      'hasMeleeAttack': true
      },
    effects: {
      'hasFood': true
      }
  },
  
  // Eat an entity from the ground, without picking it up first
  'Graze': {
    conditions: {
      'isTargetFood': true,
      'inMeleeRange': true
      },
    effects: {
      'isHungry': false
      }
  },

  'Harvest': {
    conditions: {
      'isTargetFood': true,
      'inMeleeRange': true
      },
    effects: {
      'hasFood': true
    }
  },

  'PickFoodTarget': {
    conditions: {
      'hasVisibleEntities': true,
      'isHungry': true
      },
    effects: {
      'hasTarget': true
      }
  },

  'PickFightTarget': {
    conditions: {
      'hasVisibleEntities': true,
      'hasMeleeAttack': true
      },
    effects: {
      'hasTarget': true
      }
  },

  'PickLoveTarget': {
    conditions: {
      'hasVisibleEntities': true,
      'isRutting': true
      },
    effects: {
      'hasTarget': true
      }
  },

  'Reproduce': {
    conditions: {
      'inMeleeRange': true
      },
    effects: {
      'isRutting': false
      }
  },

  // Consume an item from inventory
  'Eat': {
    conditions: {
      'hasFood': true
      },
    effects: {
      'isHungry': false
      }
  },
  
  'Sleep': {
    conditions: {
      'hasTarget': false
      },
    effects: {
      'isHealthLow': false
      }
  },
  
  'Wander': {
    conditions: {
      'hasTarget': false
      },
    effects: {
      'hasVisibleEntities': true
      }
  },
  
  'MeleeAttack': {
    conditions: {
      'inMeleeRange': true,
      'hasMeleeAttack': true
      },
    effects: {
      'isTargetDead': true
      }
  },

  'MissileAttack': {
    conditions: {
      'inMissileRange': true,
      'hasMissileAttack': true
      },
    effects: {
      'isTargetDead': true
      }
  },
  
  'ApproachTarget': {
    conditions: {
      'hasTarget': true
      },
    effects: {
      'inMeleeRange': true,
      'inMissileRange': true,
      'isTargetVisible': true
      }
  },
  
  'FleeTarget': {
    conditions: {
      'hasTarget': true,
      'hasMissileAttack': true
      },
    effects: {
      'inMeleeRange': false,
      'inMissileRange': false,
      'isTargetVisible': false
      }
  }
}
